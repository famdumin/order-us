import * as Yup from 'yup';

export function required(message) {
  return Yup.string().required(message);
}
