export const templates = {
  REQUIRED: {
    required: 'Trường này bắt buộc điền',
  },
  MIN_VALUE: (length) => ({
    min: {
      value: length,
      message: `Nhỏ nhất là ${length}`,
    },
  }),
  MAX_VALUE: (length) => ({
    max: {
      value: length,
      message: `Lớn nhất là ${length}`,
    },
  }),
  MIN_LENGTH: (length) => ({
    maxLength: {
      value: length,
      message: `Tối thiếu ${length} ký tự`,
    },
  }),
  MAX_LENGTH: (length) => ({
    maxLength: {
      value: length,
      message: `Tối đa ${length} ký tự`,
    },
  }),
  REGEX: (pattern, format) => ({
    pattern: {
      value: pattern,
      message: `Sai định dạng cho phép: ${format}`,
    },
  }),
  CUSTOM: (customValidateObject) => ({
    validate: customValidateObject,
  }),
};

export const messages = {
  requiredField: 'Trường này bắt buộc điền.',
  emailField: 'Email không hợp lệ.',
  maxLength: (number) => `Tối thiếu ${number} ký tự.`,
};
