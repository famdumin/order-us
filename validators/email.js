import * as Yup from 'yup';

import { required } from './required';

export function email(message, requiredMessage) {
  if (requiredMessage) {
    return required(requiredMessage).email(message);
  }

  return Yup.string().email(message);
}
