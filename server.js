const express = require('express');
const next = require('next');
const notifier = require('node-notifier');
// const { setConfig } = require('next/config');
// const nextI18NextMiddleware = require('next-i18next/middleware').default;
const { parse } = require('url');
const { join } = require('path');
// setConfig(require('./next.config'));

// const nextI18next = require('./utils/i18n');
const config = require('./config');

const app = next({ dev: config.IS_DEV });
const handle = app.getRequestHandler();

const rootStaticFiles = ['/robots.txt', '/sitemap.xml', '/favicon.ico'];

(async () => {
  await app.prepare();
  const server = express();
  // server.use(nextI18NextMiddleware(nextI18next));

  server.get('*', (req, res) => {
    const parsedUrl = parse(req.url, true);

    if (rootStaticFiles.indexOf(parsedUrl.pathname) > -1) {
      const path = join(__dirname, 'static', parsedUrl.pathname);

      return app.serveStatic(req, res, path);
    }

    return handle(req, res, parsedUrl);
  });

  server.listen(config.PORT, (err) => {
    if (err) {
      throw err;
    }

    notifier.notify({
      title: 'Notify',
      message: `OrderUS Website started on http://localhost:${config.PORT}`,
    });
  });
})();
