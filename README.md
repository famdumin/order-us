# OrderUS website install

## OS: Ubuntu 19.10 x64

## Install nodejs

```
apt install nodejs
```

## Install npm

```
apt install npm
```

## Install yarn

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
```

```
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```

```
apt-get update && apt-get install yarn
```

## Intall pm2

```
npm install pm2 -g
```

## Install nginx

```
apt-get install nginx
```

## Copy default nginx config to new file

Use your own hostname (staging: 139.180.223.241, prod: )

```
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/orderus-website
```

## Edit config file

Use your own hostname (staging: 139.180.223.241, prod: )

```
nano /etc/nginx/sites-available/orderus-website
```

Use your own server name (staging: 139.180.223.241, prod: ) and port (7000)

```
server {
    listen 80;
    server_name 139.180.223.241;

    location / {
        proxy_pass http://localhost:7000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

## Save config file and enable config

Use your own hostname (orderus-website)

```
ln -s /etc/nginx/sites-available/orderus-website /etc/nginx/sites-enabled/orderus-website
```

## Test config

```
service nginx configtest
```

## Restart nginx

```
service nginx restart
```

## Startup default run nginx

```
update-rc.d nginx defaults
```

## Create SSH key for Gitlab CI/CD

Use your own email (devop@cyberskill.tech)

```
ssh-keygen -t rsa -b 4096 -C "devop@cyberskill.tech"
```

Enter three times when it asks

```
copy “id_rsa.pub” file into root/.ssh/ and rename it authorized_keys
cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys
```

Use your own keyName (SSH_PRIVATE_KEY)

```
go into Gitlab => Settings => CI/CD => Environment variables => SSH_PRIVATE_KEY
create new variable with “id_rsa” file’s content
to get content of file
cat ~/.ssh/id_rsa
```

Use your own hostname (SERVER_HOST)

```
go into Gitlab => Settings => CI/CD => Environment variables => SERVER_HOST
create new variable with username@hostIP ex: root@139.180.223.241
```

```
copy “id_rsa.pub” file content into [Github SSH Keys](https://gitlab.com/profile/keys)
to get content of file
cat ~/.ssh/id_rsa.pub
```

```
add github to known_host
```

ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts

<!-- chmod 644 ~/.ssh/known_hosts -->

```

```

## create swap if it's stuck when running build

sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

- edit file fstab
  sudo nano /etc/fstab
  paste the following: /swapfile swap swap defaults 0 0

- verify
  sudo swapon --show

## For windows developers:

Install those dependencies before run yarn command  
Netframework 2.5  
Netframework 4  
Run Powershell as administrator and run "npm install --global --production windows-build-tools"
