// import { ApolloClient } from 'apollo-client';
// import { ApolloLink /*split*/ } from 'apollo-link';
// // import { WebSocketLink } from 'apollo-link-ws';
// // import { SubscriptionClient } from 'subscriptions-transport-ws';
// import { onError } from 'apollo-link-error';
// import { InMemoryCache } from 'apollo-cache-inmemory';
// import { createUploadLink } from 'apollo-upload-client';
// // import { getMainDefinition } from 'apollo-utilities';
// import fetch from 'isomorphic-fetch';

import config from '../config';

// // eslint-disable-next-line no-console
// console.log(config.GRAPHQL_HTTP_URI);

// if (!process.browser) {
//   global.fetch = fetch;
// }

// const httpLink = createUploadLink({
//   uri: config.GRAPHQL_HTTP_URI,
//   credentials: 'include',
// });

// // const wsLink = process.browser
// //     ? new WebSocketLink(
// //           new SubscriptionClient(config.GRAPHQL_WS_URI, {
// //               reconnect: true
// //           })
// //       )
// //     : null;

// // const terminatingLink = process.browser
// //     ? split(
// //           ({ query }) => {
// //               const { kind, operation } = getMainDefinition(query);
// //               return (
// //                   kind === 'OperationDefinition' && operation === 'subscription'
// //               );
// //           },
// //           wsLink,
// //           httpLink
// //       )
// //     : httpLink;

// const client = new ApolloClient({
//   connectToDevTools: process.browser,
//   ssrMode: !process.browser,
//   link: ApolloLink.from([
//     onError(({ graphQLErrors, networkError }) => {
//       if (graphQLErrors)
//         graphQLErrors.map(({ message, locations, path }) =>
//           // eslint-disable-next-line
//           console.error(
//             `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
//           )
//         );
//       // eslint-disable-next-line
//       if (networkError) console.error(`[Network error]: ${networkError}`);
//     }),
//     // terminatingLink
//     httpLink,
//   ]),
//   cache: new InMemoryCache(),
//   // defaultOptions: {
//   //   watchQuery: {
//   //     fetchPolicy: 'cache-and-network'
//   //   },
//   //   query: {
//   //     fetchPolicy: 'no-cache'
//   //   }
//   // }
// });

// export default client;

import { useMemo } from 'react';
import { ApolloClient, InMemoryCache, HttpLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';

let apolloClient;

function createApolloClient() {
  const httpLink = new HttpLink({
    uri: config.GRAPHQL_HTTP_URI,
    credentials: 'include',
  });
  const error = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path }) =>
        console.error(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
      );
    if (networkError) console.error(`[Network error]: ${networkError}`);
  });
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: error.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true,
  });
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();
  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    _apolloClient.cache.restore(initialState);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function useApollo(initialState) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
