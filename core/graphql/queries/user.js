import gql from 'graphql-tag';

export default {
  query: {
    getUsers: gql`
      query($filters: InputPagingRequest) {
        getUsers(filters: $filters) {
          success
          message
          result {
            docs {
              id
              username
              avatar
              fullName
              email
              phone
              deliveryLocation
              isWholeSale
              roleId
              permissionId
              role {
                id
                name
                isDel
              }
              permissions {
                id
                name
                isDel
              }
              isDel
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getUser: gql`
      query($user: InputUserGet!) {
        getUser(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
  },
  mutation: {
    createUser: gql`
      mutation($user: InputUserCreate!) {
        createUser(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
    updateUser: gql`
      mutation($user: InputUserUpdate!) {
        updateUser(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
    deleteUser: gql`
      mutation($user: InputUserDeleteRecover!) {
        deleteUser(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
    recoverUser: gql`
      mutation($user: InputUserDeleteRecover!) {
        recoverUser(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
    changePass: gql`
      mutation($user: InputUserChangePass!) {
        changePass(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
    forgotPass: gql`
      mutation($user: InputUserForgotPass!) {
        forgotPass(user: $user) {
          success
          message
        }
      }
    `,
    registerUserWholeSale: gql`
      mutation {
        registerUserWholeSale {
          success
        }
      }
    `,
    verifyResetPassToken: gql`
      mutation($user: InputUserVerifyResetPassToken!) {
        verifyResetPassToken(user: $user) {
          success
          result {
            username
          }
        }
      }
    `,
    updatePassViaMail: gql`
      mutation($user: InputUserUpdatePassViaMail!) {
        updatePassViaMail(user: $user) {
          success
          message
          result {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
        }
      }
    `,
  },
};
