import gql from 'graphql-tag';

export default {
  query: {
    checkAuth: gql`
      query($token: InputAuthToken) {
        checkAuth(token: $token) {
          success
          user {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
          role {
            name
          }
          token
        }
      }
    `,

    checkAuthAdmin: gql`
      query($token: InputAuthToken) {
        checkAuthAdmin(token: $token) {
          success
          user {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
          role {
            name
          }
          permission {
            id
            name
          }
          token
        }
      }
    `,
  },
  mutation: {
    login: gql`
      mutation($user: InputAuthLogin!) {
        login(user: $user) {
          success
          user {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
          role {
            name
          }
          token
        }
      }
    `,
    loginAdmin: gql`
      mutation($user: InputAuthLogin!) {
        loginAdmin(user: $user) {
          success
          user {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
          role {
            name
          }
          permission {
            name
          }
          token
        }
      }
    `,
    register: gql`
      mutation($user: InputAuthRegister!) {
        register(user: $user) {
          success
          user {
            id
            username
            avatar
            fullName
            email
            phone
            deliveryLocation
            isWholeSale
            roleId
            permissionId
            isDel
          }
          role {
            name
          }
          token
        }
      }
    `,
    logout: gql`
      mutation {
        logout {
          success
        }
      }
    `,
  },
};
