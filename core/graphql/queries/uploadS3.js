import gql from 'graphql-tag';

export default {
  query: {},
  mutation: {
    uploadS3: gql`
      mutation($upload: InputUploadS3Create!) {
        uploadS3(upload: $upload) {
          success
          message
          result {
            images
          }
        }
      }
    `,
  },
};
