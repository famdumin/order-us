import gql from 'graphql-tag';

export default {
  query: {
    getAdvertisementProducts: gql`
      query($filters: InputPagingRequest) {
        getAdvertisementProducts(filters: $filters) {
          success
          message
          result {
            docs {
              id
              slug
              name
              categoryIds
              images
              type
              price
              availableForShopping
              hasShippingFee
              estimateShippingTimeFrom
              estimateShippingTimeTo
              isSpecialOffer
              partnerId
              partner {
                id
                name
                address
                phone
                email
                createdAt
                updatedAt
                isDel
              }
              schedulers {
                id
                name
                advertisementProductType
                discountType
                discountValue
                startTime
                endTime
                selectedDay
                advertisementProductIds
                isSpecialOffer
                createdAt
                updatedAt
                isDel
              }
              attributes {
                name
                type
                value
              }
            }
            totalDocs
            limit
            page
            totalPages
          }
        }
      }
    `,
    getAdvertisementProduct: gql`
      query($advertisementProduct: InputAdvertisementProductGet!) {
        getAdvertisementProduct(advertisementProduct: $advertisementProduct) {
          success
          result {
            id
            slug
            name
            categoryIds
            images
            type
            price
            availableForShopping
            hasShippingFee
            shippingFee
            estimateShippingTimeFrom
            estimateShippingTimeTo
            partnerId
            partner {
              id
              name
              address
              phone
              email
              createdAt
              updatedAt
              isDel
            }
            schedulers {
              id
              name
              advertisementProductType
              discountType
              discountValue
              startTime
              endTime
              selectedDay
              advertisementProductIds
              createdAt
              updatedAt
              isDel
            }
            attributes {
              name
              type
              value
            }
            comboProducts {
              name
              items {
                productId
                product {
                  id
                  slug
                  name
                  images
                  type
                  price
                  attributes {
                    name
                    type
                    value
                  }
                  schedulers {
                    id
                    name
                    advertisementProductType
                    discountType
                    discountValue
                    startTime
                    endTime
                    selectedDay
                    advertisementProductIds
                    isSpecialOffer
                    createdAt
                    updatedAt
                    isDel
                  }
                }
                attributes {
                  name
                  value
                }
                schedulers {
                  id
                  name
                  advertisementProductType
                  discountType
                  discountValue
                  startTime
                  endTime
                  selectedDay
                  advertisementProductIds
                  createdAt
                  updatedAt
                  isDel
                }
                # isFree
              }
              # originalPrice
              # isCustomPrice
              # customPrice
            }
            customerContacts {
              id
              productId
              name
              note
              phone
              email
              createdAt
              updatedAt
              isDel
            }
            description
            moreInformation
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
  },
  mutation: {
    createAdvertisementProduct: gql`
      mutation($advertisementProduct: InputAdvertisementProductCreate!) {
        createAdvertisementProduct(advertisementProduct: $advertisementProduct) {
          success
          message
          result {
            name
            categoryIds
            images
            type
            price
            availableForShopping
            hasShippingFee
            shippingFee
            estimateShippingTimeFrom
            estimateShippingTimeTo
            partnerId
            partner {
              name
              address
            }
            schedulers {
              name
              advertisementProductType
              discountType
              discountValue
              startTime
              endTime
              selectedDay
              advertisementProductIds
            }
          }
        }
      }
    `,
    updateAdvertisementProduct: gql`
      mutation($advertisementProduct: InputAdvertisementProductUpdate!) {
        updateAdvertisementProduct(advertisementProduct: $advertisementProduct) {
          success
          message
          result {
            id
            name
            categoryIds
            images
            type
            price
            availableForShopping
            hasShippingFee
            shippingFee
            estimateShippingTimeFrom
            estimateShippingTimeTo
            partnerId
            attributes {
              name
              type
              value
            }
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    deleteAdvertisementProduct: gql`
      mutation($advertisementProduct: InputAdvertisementProductDeleteRecover!) {
        deleteAdvertisementProduct(advertisementProduct: $advertisementProduct) {
          success
          message
          result {
            id
            name
            categoryIds
            images
            type
            price
            availableForShopping
            hasShippingFee
            shippingFee
            estimateShippingTimeFrom
            estimateShippingTimeTo
            partnerId
            attributes {
              name
              type
              value
            }
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    createAdvertisementProductScheduler: gql`
      mutation($advertisementProductScheduler: InputAdvertisementProductSchedulerCreate!) {
        createAdvertisementProductScheduler(
          advertisementProductScheduler: $advertisementProductScheduler
        ) {
          success
          message
          result {
            id
            name
            advertisementProductType
            discountType
            discountValue
            startTime
            endTime
            selectedDay
            advertisementProductIds
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
  },
};
