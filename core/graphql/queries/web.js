import gql from 'graphql-tag';

import { webFragments } from './fragments';

export default {
  query: {
    getWebs: gql`
      ${webFragments.webDetail}
      query($filters: InputPagingRequest) {
        getWebs(filters: $filters) {
          success
          message
          result {
            docs {
              ...WebDetail
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getWeb: gql`
      ${webFragments.webDetail}
      query($web: InputWebGet!) {
        getWeb(web: $web) {
          success
          message
          result {
            ...WebDetail
          }
        }
      }
    `,
  },
  mutation: {
    createWeb: gql`
      ${webFragments.webDetail}
      mutation($web: InputWebCreate!) {
        createWeb(web: $web) {
          success
          message
          result {
            ...WebDetail
          }
        }
      }
    `,
    updateWeb: gql`
      ${webFragments.webDetail}
      mutation($web: InputWebUpdate!) {
        updateWeb(web: $web) {
          success
          message
          result {
            ...WebDetail
          }
        }
      }
    `,
    deleteWeb: gql`
      ${webFragments.webDetail}
      mutation($web: InputWebDeleteRecover!) {
        deleteWeb(web: $web) {
          success
          message
          result {
            ...WebDetail
          }
        }
      }
    `,
    recoverWeb: gql`
      ${webFragments.webDetail}
      mutation($web: InputWebDeleteRecover!) {
        recoverWeb(web: $web) {
          success
          message
          result {
            ...WebDetail
          }
        }
      }
    `,
  },
};
