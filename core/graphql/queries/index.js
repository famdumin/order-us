import uploadS3 from './uploadS3';
import Setting from './setting';
import Auth from './auth';
import User from './user';
import Banner from './banner';
import ProductCatgory from './productCategory';
import Web from './web';
import Product from './product';
import Cart from './cart';
import Order from './order';
import AdCart from './adCart';

export const query = {
  ...Setting.query,
  ...Auth.query,
  ...User.query,
  ...Banner.query,
  ...ProductCatgory.query,
  ...Web.query,
  ...Product.query,
  ...Cart.query,
  ...Order.query,
  ...AdCart.query,
};

export const mutation = {
  ...uploadS3.mutation,
  ...Setting.mutation,
  ...Auth.mutation,
  ...User.mutation,
  ...Banner.mutation,
  ...ProductCatgory.mutation,
  ...Web.mutation,
  ...Product.mutation,
  ...Cart.mutation,
  ...Order.mutation,
  ...AdCart.mutation,
};

export const subscription = {
  //   ...Notification.subscription
};
