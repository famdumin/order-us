import gql from 'graphql-tag';

import { orderFragments } from './fragments';

export default {
  query: {
    getOrder: gql`
      ${orderFragments.user}
      ${orderFragments.staff}
      ${orderFragments.staffInfo}
      query($order: InputOrderGet!) {
        getOrder(order: $order) {
          success
          result {
            id
            userId
            user {
              ...OrderUser
            }
            orderDate
            complains {
              id
              complainNote
              resolveNote
              isResolved
            }
            fee {
              refund
              total
              subTotal
              shippingFeeAtPurchasePlace
              shippingFeeToReceivePlace
              purchaseFee
              paid
            }
            staff {
              ...OrderStaff
              ...OrderStaffInfo
            }
            status
            deliveryLocation
          }
        }
      }
    `,
    getAdOrder: gql`
      query($order: InputAdOrderGet!) {
        getAdOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              subTotal
              total
              paid
              shippingFee
              discount
            }
            staff {
              customerCareStaff
              warehouseStaff
              customerCare {
                id
                username
                fullName
                avatar
                email
                phone
                deliveryLocation
                isWholeSale
                createdAt
                updatedAt
                isDel
              }
              warehouse {
                id
                username
                fullName
                avatar
                email
                phone
                deliveryLocation
                isWholeSale
                createdAt
                updatedAt
                isDel
              }
            }
            deliveryLocation
            status
            complainIds
            complains {
              id
              complainNote
              resolveNote
              isResolved
              createdAt
              updatedAt
            }
            infoVA {
              response_code
              message
              account_no
              account_name
              bank_code
              bank_name
              map_id
              request_id
              start_date
              end_date
              amount
              transactionType
              description
            }
            createdAt
            updatedAt
            isDel
            user {
              id
              username
              fullName
              avatar
              email
              phone
              deliveryLocation
              isWholeSale
              createdAt
              updatedAt
              isDel
            }
          }
        }
      }
    `,
    getAllOrders: gql`
      ${orderFragments.user}
      ${orderFragments.adUser}
      ${orderFragments.staff}
      ${orderFragments.adStaff}
      ${orderFragments.staffInfo}
      ${orderFragments.adStaffInfo}
      query($filters: InputPagingRequest) {
        getAllOrders(filters: $filters) {
          success
          message
          result {
            docs {
              ... on Order {
                Kind
                id
                userId
                orderDate
                complains {
                  id
                  complainNote
                  resolveNote
                  isResolved
                }
                fee {
                  refund
                  paid
                  total
                  subTotal
                  purchaseFee
                  shippingFeeToReceivePlace
                  shippingFeeAtPurchasePlace
                }
                staff {
                  ...OrderStaff
                  ...OrderStaffInfo
                }
                deliveryLocation
                status
                createdAt
                updatedAt
                isDel
                user {
                  ...OrderUser
                }
                infoVA {
                  response_code
                  message
                  account_no
                  account_name
                  bank_code
                  bank_name
                  map_id
                  request_id
                  start_date
                  end_date
                  amount
                  transactionType
                  description
                }
              }
              ... on AdOrder {
                Kind
                id
                userId
                orderDate
                complains {
                  id
                  complainNote
                  resolveNote
                  isResolved
                }
                fee {
                  paid
                  total
                  subTotal
                  shippingFee
                  discount
                }
                staff {
                  ...AdOrderStaff
                  ...AdOrderStaffInfo
                }
                deliveryLocation
                status
                createdAt
                updatedAt
                isDel
                user {
                  ...AdOrderUser
                }
                infoVA {
                  response_code
                  message
                  account_no
                  account_name
                  bank_code
                  bank_name
                  map_id
                  request_id
                  start_date
                  end_date
                  amount
                  transactionType
                  description
                }
              }
            }
            totalDocs
            totalPages
            limit
            hasPrevPage
            hasNextPage
            page
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getOrders: gql`
      ${orderFragments.user}
      ${orderFragments.staff}
      ${orderFragments.staffInfo}
      query($filters: InputPagingRequest) {
        getOrders(filters: $filters) {
          success
          message
          result {
            docs {
              id
              userId
              orderDate
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                refund
                paid
                total
                subTotal
                purchaseFee
                shippingFeeToReceivePlace
                shippingFeeAtPurchasePlace
              }
              staff {
                ...OrderStaff
                ...OrderStaffInfo
              }
              deliveryLocation
              status
              createdAt
              updatedAt
              isDel
              user {
                ...OrderUser
              }
              infoVA {
                response_code
                message
                account_no
                account_name
                bank_code
                bank_name
                map_id
                request_id
                start_date
                end_date
                amount
                transactionType
                description
              }
            }
            totalDocs
            totalPages
            limit
            hasPrevPage
            hasNextPage
            page
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getOrderDetails: gql`
      query($filters: InputPagingRequest) {
        getOrderDetails(filters: $filters) {
          success
          result {
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
            docs {
              id
              orderId
              webId
              product {
                productInfo
                productLink
                image
                price
                amount
                weight
                currencyUnit
              }
              fee {
                subTotal
                shippingFeeToReceivePlace
                shippingFeeToReceivePlacePerWeb
                subTotalWeb
              }
              status
              customerNote
              createdAt
              updatedAt
              isDel
            }
          }
        }
      }
    `,
    getAdOrders: gql`
      ${orderFragments.adUser}
      ${orderFragments.adStaff}
      ${orderFragments.adStaffInfo}
      query($filters: InputPagingRequest) {
        getAdOrders(filters: $filters) {
          success
          message
          result {
            docs {
              id
              userId
              orderDate
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                paid
                total
                subTotal
                shippingFee
                discount
              }
              staff {
                ...AdOrderStaff
                ...AdOrderStaffInfo
              }
              deliveryLocation
              status
              createdAt
              updatedAt
              isDel
              user {
                ...AdOrderUser
              }
              infoVA {
                response_code
                message
                account_no
                account_name
                bank_code
                bank_name
                map_id
                request_id
                start_date
                end_date
                amount
                transactionType
                description
              }
            }
            totalDocs
            totalPages
            limit
            hasPrevPage
            hasNextPage
            page
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getAdOrderDetails: gql`
      query($filters: InputPagingRequest) {
        getAdOrderDetails(filters: $filters) {
          success
          result {
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
            docs {
              id
              orderId
              productId
              product {
                id
                slug
                name
                categoryIds
                images
                type
                price
                availableForShopping
                hasShippingFee
                estimateShippingTimeFrom
                estimateShippingTimeTo
                isSpecialOffer
                partnerId
                partner {
                  id
                  name
                  address
                  phone
                  email
                  createdAt
                  updatedAt
                  isDel
                }
                schedulers {
                  id
                  name
                  advertisementProductType
                  discountType
                  discountValue
                  startTime
                  endTime
                  selectedDay
                  advertisementProductIds
                  isSpecialOffer
                  createdAt
                  updatedAt
                  isDel
                }
                attributes {
                  name
                  type
                  value
                }
              }
              amount
              fee {
                shippingFee
                subTotal
                discount
              }
              attributes {
                name
                value
              }
              # status
              customerNote
              createdAt
              updatedAt
              isDel
            }
          }
        }
      }
    `,
    getOrderHistory: gql`
      ${orderFragments.user}
      ${orderFragments.staff}
      ${orderFragments.staffInfo}
      query($filters: InputPagingRequest) {
        getOrderHistory(filters: $filters) {
          success
          message
          result {
            docs {
              id
              userId
              orderDate
              complainIds
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                paid
                total
                subTotal
                purchaseFee
                shippingFeeToReceivePlace
                shippingFeeAtPurchasePlace
              }
              staff {
                ...OrderStaff
                ...OrderStaffInfo
              }
              deliveryLocation
              status
              createdAt
              updatedAt
              isDel
              user {
                ...OrderUser
              }
              changedBy {
                ...OrderUser
              }
              historyCreatedAt
              historyUpdatedAt
              orderDetails {
                id
                orderId
                webId
                product {
                  productInfo
                  productLink
                  image
                  price
                  amount
                  weight
                  currencyUnit
                }
                fee {
                  subTotal
                  shippingFeeToReceivePlace
                  shippingFeeToReceivePlacePerWeb
                  subTotalWeb
                }
                status
                customerNote
                createdAt
                updatedAt
                isDel
              }
            }
            totalDocs
            totalPages
            limit
            hasPrevPage
            hasNextPage
            page
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
  },
  mutation: {
    createOrder: gql`
      ${orderFragments.staff}
      mutation($order: InputOrderCreate) {
        createOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    createOrderDetail: gql`
      mutation($orderDetail: InputOrderDetailCreate!) {
        createOrderDetail(orderDetail: $orderDetail) {
          success
          result {
            id
            orderId
            webId
            product {
              price
              amount
              productInfo
              productLink
            }
            fee {
              subTotal
            }
            status
            customerNote
            isDel
          }
        }
      }
    `,
    createAdOrderDetail: gql`
      mutation($orderDetail: InputAdOrderDetailCreate!) {
        createAdOrderDetail(orderDetail: $orderDetail) {
          success
          message
          result {
            id
            orderId
            productId
            amount
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    updateOrder: gql`
      ${orderFragments.staff}
      mutation($order: InputOrderUpdate!) {
        updateOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    updateAdOrder: gql`
      mutation($order: InputAdOrderUpdate!) {
        updateAdOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              subTotal
              total
              paid
              shippingFee
              discount
            }
            staff {
              customerCareStaff
              warehouseStaff
              customerCare {
                id
                username
                fullName
                avatar
                email
                phone
                deliveryLocation
                isWholeSale
                createdAt
                updatedAt
                isDel
              }
              warehouse {
                id
                username
                fullName
                avatar
                email
                phone
                deliveryLocation
                isWholeSale
                createdAt
                updatedAt
                isDel
              }
            }
            deliveryLocation
            status
            complainIds
            complains {
              id
              complainNote
              resolveNote
              isResolved
              createdAt
              updatedAt
            }
            infoVA {
              response_code
              message
              account_no
              account_name
              bank_code
              bank_name
              map_id
              request_id
              start_date
              end_date
              amount
              transactionType
              description
            }
            createdAt
            updatedAt
            isDel
            user {
              id
              username
              fullName
              avatar
              email
              phone
              deliveryLocation
              isWholeSale
              createdAt
              updatedAt
              isDel
            }
          }
        }
      }
    `,
    updateOrderDetail: gql`
      mutation($orderDetail: InputOrderDetailUpdate!) {
        updateOrderDetail(orderDetail: $orderDetail) {
          success
          result {
            id
            orderId
            product {
              price
              amount
              weight
            }
            fee {
              subTotal
            }
            status
            customerNote
            isDel
          }
        }
      }
    `,
    deleteOrder: gql`
      ${orderFragments.staff}
      mutation($order: InputOrderDelete) {
        deleteOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    recoverOrder: gql`
      ${orderFragments.staff}
      mutation($order: InputOrderDelete) {
        recoverOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    createUserOrderComplain: gql`
      ${orderFragments.staff}
      mutation($order: InputUserOrderComplain!) {
        createUserOrderComplain(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    userShippingFeeOrderPayment: gql`
      ${orderFragments.staff}
      mutation($order: InputUserShippingFeeOrderPayment!) {
        userShippingFeeOrderPayment(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    userShippingFeeOrderPaymentVA: gql`
      mutation($order: InputUserShippingFeeOrderPayment!, $infoVA: InputInfoVA!) {
        userShippingFeeOrderPaymentVA(order: $order, infoVA: $infoVA) {
          success
          message
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
    cancelOrder: gql`
      ${orderFragments.staff}
      mutation($order: InputOrderCancel!) {
        cancelOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    cancelAdOrder: gql`
      mutation($order: InputAdOrderCancel!) {
        cancelAdOrder(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              subTotal
              total
              paid
              shippingFee
              discount
            }
            deliveryLocation
            status
            complainIds
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    cancelOrderDetail: gql`
      mutation($orderDetail: InputOrderDetailCancel!) {
        cancelOrderDetail(orderDetail: $orderDetail) {
          success
          message
          result {
            id
            orderId
            webId
            product {
              productInfo
              productLink
              image
              price
              amount
            }
            fee {
              subTotal
              shippingFeeToReceivePlace
              shippingFeeToReceivePlacePerWeb
              subTotalWeb
            }
            status
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    resolveOrderComplain: gql`
      ${orderFragments.staff}
      mutation($order: InputResolveOrderComplain!) {
        resolveOrderComplain(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              ...OrderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    userShippingFeeOrderDetailsPayment: gql`
      mutation($orderDetail: InputUserShippingFeeOrderDetailsPayment!) {
        userShippingFeeOrderDetailsPayment(orderDetail: $orderDetail) {
          success
        }
      }
    `,
    userShippingFeeOrderDetailsPaymentVA: gql`
      mutation($orderDetail: InputUserShippingFeeOrderDetailsPayment!, $infoVA: InputInfoVA!) {
        userShippingFeeOrderDetailsPayment(orderDetail: $orderDetail, infoVA: $infoVA) {
          success
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
    userOrderPayment: gql`
      mutation($order: InputUserOrderPayment!) {
        userOrderPayment(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              customerCareStaff
              warehouseStaff
              orderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    userOrderPaymentVA: gql`
      mutation($order: InputUserOrderPayment!, $infoVA: InputInfoVA!) {
        userOrderPaymentVA(order: $order, infoVA: $infoVA) {
          success
          message
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
    adminOrderRefund: gql`
      mutation($order: InputAdminOrderRefund!) {
        adminOrderRefund(order: $order) {
          success
          message
        }
      }
    `,
  },
};
