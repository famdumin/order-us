import gql from 'graphql-tag';

import { orderFragments } from './fragments';

export default {
  query: {
    getAllCart: gql`
      ${orderFragments.user}
      ${orderFragments.adUser}
      ${orderFragments.staff}
      ${orderFragments.adStaff}
      ${orderFragments.staffInfo}
      ${orderFragments.adStaffInfo}
      query($filters: InputPagingRequest) {
        getAllCart(filters: $filters) {
          success
          message
          result {
            docs {
              ... on OrderDetail {
                Kind
                id
                orderId
                webId
                product {
                  productInfo
                  productLink
                  image
                  weight
                  price
                  amount
                  currencyUnit
                }
                fee {
                  subTotal
                  subTotalWeb
                  shippingFeeToReceivePlace
                  shippingFeeToReceivePlacePerWeb
                }
                status
                customerNote
                createdAt
                updatedAt
                isDel
              }
              ... on AdOrderDetail {
                Kind
                id
                orderId
                productId
                product {
                  id
                  name
                  categoryIds
                  images
                  slug
                  type
                  price
                  hasShippingFee
                  shippingFee
                  partnerId
                  schedulers {
                    id
                    name
                    advertisementProductType
                    discountType
                    discountValue
                    startTime
                    endTime
                    selectedDay
                    advertisementProductIds
                    createdAt
                    updatedAt
                    isDel
                  }
                  attributes {
                    name
                    type
                    value
                  }
                  comboProducts {
                    name
                    items {
                      productId
                      # isFree
                    }
                    # originalPrice
                    # isCustomPrice
                    # customPrice
                  }
                  description
                  moreInformation
                  availableForShopping
                  slug
                  estimateShippingTimeFrom
                  estimateShippingTimeTo
                  isSpecialOffer
                  createdAt
                  updatedAt
                  isDel
                }
                amount
                fee {
                  shippingFee
                  subTotal
                  discount
                }
                attributes {
                  name
                  value
                }
                customerNote
                createdAt
                updatedAt
                isDel
              }
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
            order {
              id
              userId
              user {
                ...OrderUser
              }
              orderDate
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                total
                subTotal
                shippingFeeAtPurchasePlace
                shippingFeeToReceivePlace
                purchaseFee
                paid
              }
              staff {
                ...OrderStaff
                ...OrderStaffInfo
              }
              status
              deliveryLocation
            }
            adOrder {
              id
              userId
              user {
                ...AdOrderUser
              }
              orderDate
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                total
                subTotal
                paid
                shippingFee
                discount
              }
              staff {
                ...AdOrderStaff
                ...AdOrderStaffInfo
              }
              status
              deliveryLocation
            }
          }
        }
      }
    `,
    getCart: gql`
      ${orderFragments.user}
      ${orderFragments.staff}
      ${orderFragments.staffInfo}
      query($filters: InputPagingRequest) {
        getCart(filters: $filters) {
          success
          message
          result {
            docs {
              id
              orderId
              webId
              product {
                productInfo
                productLink
                image
                weight
                price
                amount
                currencyUnit
              }
              fee {
                subTotal
                subTotalWeb
                shippingFeeToReceivePlace
                shippingFeeToReceivePlacePerWeb
              }
              status
              customerNote
              createdAt
              updatedAt
              isDel
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
            order {
              id
              userId
              user {
                ...OrderUser
              }
              orderDate
              complains {
                id
                complainNote
                resolveNote
                isResolved
              }
              fee {
                total
                subTotal
                shippingFeeAtPurchasePlace
                shippingFeeToReceivePlace
                purchaseFee
                paid
              }
              staff {
                ...OrderStaff
                ...OrderStaffInfo
              }
              status
              deliveryLocation
            }
          }
        }
      }
    `,
  },
  mutation: {
    addCart: gql`
      mutation($cart: [InputCartCreate]!) {
        addCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            webId
            product {
              productInfo
              productLink
              image
              weight
              price
              amount
              currencyUnit
            }
            fee {
              shippingFeeToReceivePlace
              shippingFeeToReceivePlacePerWeb
              subTotal
              subTotalWeb
            }
            status
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    updateCart: gql`
      mutation($cart: InputCartUpdate!) {
        updateCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            webId
            product {
              productInfo
              productLink
              image
              weight
              price
              amount
              currencyUnit
            }
            fee {
              shippingFeeToReceivePlace
              shippingFeeToReceivePlacePerWeb
              subTotal
              subTotalWeb
            }
            status
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    deleteAllCart: gql`
      mutation {
        deleteAllCart {
          success
          message
          result {
            id
            orderId
            webId
            product {
              productInfo
              productLink
              image
              weight
              price
              amount
              currencyUnit
            }
            fee {
              shippingFeeToReceivePlace
              shippingFeeToReceivePlacePerWeb
              subTotal
              subTotalWeb
            }
            status
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    deleteCart: gql`
      mutation($cart: InputCartDelete!) {
        deleteCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            webId
            product {
              productInfo
              productLink
              image
              weight
              price
              amount
              currencyUnit
            }
            fee {
              shippingFeeToReceivePlace
              shippingFeeToReceivePlacePerWeb
              subTotal
              subTotalWeb
            }
            status
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    cartPayment: gql`
      mutation($order: InputCartPayment!) {
        cartPayment(order: $order) {
          success
          message
          result {
            id
            userId
            orderDate
            fee {
              total
              subTotal
              purchaseFee
              shippingFeeToReceivePlace
              shippingFeeAtPurchasePlace
            }
            staff {
              customerCareStaff
              warehouseStaff
              orderStaff
            }
            deliveryLocation
            status
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    cartPaymentVA: gql`
      mutation($order: InputCartPayment!, $infoVA: InputInfoVA!) {
        cartPaymentVA(order: $order, infoVA: $infoVA) {
          success
          message
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
    cartPaymentAllVA: gql`
      mutation($order: InputCartPayment!, $infoVA: InputInfoVA!) {
        cartPaymentAllVA(order: $order, infoVA: $infoVA) {
          success
          message
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
  },
};
