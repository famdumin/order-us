import gql from 'graphql-tag';

export default {
  query: {
    getSettings: gql`
      query {
        getSettings {
          success
          message
          result {
            id
            name
            value
          }
        }
      }
    `,
    getCartCount: gql`
      query {
        getCartCount {
          success
          result {
            orderCount
            adOrderCount
          }
        }
      }
    `,
  },
  mutation: {
    updateSettings: gql`
      mutation($settings: [InputSettingUpdate]!) {
        updateSettings(settings: $settings) {
          success
          message
          result {
            id
            name
            value
          }
        }
      }
    `,
  },
};
