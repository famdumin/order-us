import gql from 'graphql-tag';

import { orderFragments } from './fragments';

export default {
  query: {
    getAdCart: gql`
      query($filters: InputPagingRequest) {
        getAdCart(filters: $filters) {
          success
          message
          result {
            docs {
              id
              orderId
              productId
              product {
                id
                name
                categoryIds
                images
                slug
                type
                price
                hasShippingFee
                shippingFee
                partnerId
                schedulers {
                  id
                  name
                  advertisementProductType
                  discountType
                  discountValue
                  startTime
                  endTime
                  selectedDay
                  advertisementProductIds
                  createdAt
                  updatedAt
                  isDel
                }
                attributes {
                  name
                  type
                  value
                }
                comboProducts {
                  name
                  items {
                    productId
                    # isFree
                  }
                  # originalPrice
                  # isCustomPrice
                  # customPrice
                }
                description
                moreInformation
                availableForShopping
                slug
                estimateShippingTimeFrom
                estimateShippingTimeTo
                isSpecialOffer
                createdAt
                updatedAt
                isDel
              }
              amount
              fee {
                shippingFee
                subTotal
                discount
              }
              attributes {
                name
                value
              }
              customerNote
              createdAt
              updatedAt
              isDel
            }
            limit
            page
            totalPages
            order {
              id
              userId
              orderDate
              fee {
                subTotal
                total
                paid
                shippingFee
                discount
              }
              staff {
                customerCareStaff
                warehouseStaff
                customerCare {
                  id
                  username
                  fullName
                  avatar
                  email
                  phone
                  deliveryLocation
                  isWholeSale
                  createdAt
                  updatedAt
                  isDel
                }
                warehouse {
                  id
                  username
                  fullName
                  avatar
                  email
                  phone
                  deliveryLocation
                  isWholeSale
                  createdAt
                  updatedAt
                  isDel
                }
              }
              deliveryLocation
              status
              complainIds
              complains {
                id
                complainNote
                resolveNote
                isResolved
                createdAt
                updatedAt
              }
              infoVA {
                response_code
                message
                account_no
                account_name
                bank_code
                bank_name
                map_id
                request_id
                start_date
                end_date
                amount
                transactionType
                description
              }
              createdAt
              updatedAt
              isDel
              user {
                id
                username
                fullName
                avatar
                email
                phone
                deliveryLocation
                isWholeSale
                createdAt
                updatedAt
                isDel
              }
            }
          }
        }
      }
    `,
  },
  mutation: {
    addAdCart: gql`
      mutation($cart: [InputAdCartCreate]!) {
        addAdCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            productId
            product {
              id
              slug
              name
              categoryIds
              images
              type
              price
              availableForShopping
              hasShippingFee
              partnerId
              partner {
                id
                name
                address
                phone
                email
                createdAt
                updatedAt
                isDel
              }
              schedulers {
                id
                name
                advertisementProductType
                discountType
                discountValue
                startTime
                endTime
                selectedDay
                advertisementProductIds
                createdAt
                updatedAt
                isDel
              }
              attributes {
                name
                type
                value
              }
            }
            amount
            fee {
              shippingFee
              subTotal
              discount
            }
            attributes {
              name
              value
            }
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    updateAdCart: gql`
      mutation($cart: InputAdCartUpdate!) {
        updateAdCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            productId
            product {
              id
              slug
              name
              categoryIds
              images
              type
              price
              availableForShopping
              hasShippingFee
              partnerId
              partner {
                id
                name
                address
                phone
                email
                createdAt
                updatedAt
                isDel
              }
              schedulers {
                id
                name
                advertisementProductType
                discountType
                discountValue
                startTime
                endTime
                selectedDay
                advertisementProductIds
                createdAt
                updatedAt
                isDel
              }
              attributes {
                name
                type
                value
              }
            }
            amount
            fee {
              shippingFee
              subTotal
              discount
            }
            attributes {
              name
              value
            }
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    deleteAllAdCart: gql`
      mutation {
        deleteAllAdCart {
          success
          message
          result {
            id
            orderId
            productId
            product {
              id
              slug
              name
              categoryIds
              images
              type
              price
              availableForShopping
              hasShippingFee
              partnerId
              partner {
                id
                name
                address
                phone
                email
                createdAt
                updatedAt
                isDel
              }
              schedulers {
                id
                name
                advertisementProductType
                discountType
                discountValue
                startTime
                endTime
                selectedDay
                advertisementProductIds
                createdAt
                updatedAt
                isDel
              }
              attributes {
                name
                type
                value
              }
            }
            amount
            fee {
              shippingFee
              subTotal
              discount
            }
            attributes {
              name
              value
            }
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    deleteAdCart: gql`
      mutation($cart: InputAdCartDelete!) {
        deleteAdCart(cart: $cart) {
          success
          message
          result {
            id
            orderId
            productId
            product {
              id
              slug
              name
              categoryIds
              images
              type
              price
              availableForShopping
              hasShippingFee
              partnerId
              partner {
                id
                name
                address
                phone
                email
                createdAt
                updatedAt
                isDel
              }
              schedulers {
                id
                name
                advertisementProductType
                discountType
                discountValue
                startTime
                endTime
                selectedDay
                advertisementProductIds
                createdAt
                updatedAt
                isDel
              }
              attributes {
                name
                type
                value
              }
            }
            amount
            fee {
              shippingFee
              subTotal
              discount
            }
            attributes {
              name
              value
            }
            customerNote
            createdAt
            updatedAt
            isDel
          }
        }
      }
    `,
    cartPaymentAdOrderVA: gql`
      mutation($order: InputAdOrderCartPayment!, $infoVA: InputInfoVA!) {
        cartPaymentAdOrderVA(order: $order, infoVA: $infoVA) {
          success
          message
          result {
            response_code
            message
            account_no
            account_name
            bank_code
            bank_name
            map_id
            request_id
            start_date
            end_date
            amount
            transactionType
            description
          }
        }
      }
    `,
  },
};
