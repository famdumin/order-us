import gql from 'graphql-tag';

import { bannerFragments } from './fragments';

export default {
  query: {
    getBanners: gql`
      ${bannerFragments.bannerDetail}
      query($filters: InputPagingRequest) {
        getBanners(filters: $filters) {
          success
          message
          result {
            docs {
              ...BannerDetail
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getBanner: gql`
      ${bannerFragments.bannerDetail}
      query($banner: InputBannerGet!) {
        getBanner(banner: $banner) {
          success
          message
          result {
            ...BannerDetail
          }
        }
      }
    `,
  },
  mutation: {
    createBanner: gql`
      ${bannerFragments.bannerDetail}
      mutation($banner: InputBannerCreate!) {
        createBanner(banner: $banner) {
          success
          message
          result {
            ...BannerDetail
          }
        }
      }
    `,
    updateBanner: gql`
      ${bannerFragments.bannerDetail}
      mutation($banner: InputBannerUpdate!) {
        updateBanner(banner: $banner) {
          success
          message
          result {
            ...BannerDetail
          }
        }
      }
    `,
    deleteBanner: gql`
      ${bannerFragments.bannerDetail}
      mutation($banner: InputBannerDeleteRecover!) {
        deleteBanner(banner: $banner) {
          success
          message
          result {
            ...BannerDetail
          }
        }
      }
    `,
    recoverBanner: gql`
      ${bannerFragments.bannerDetail}
      mutation($banner: InputBannerDeleteRecover!) {
        recoverBanner(banner: $banner) {
          success
          message
          result {
            ...BannerDetail
          }
        }
      }
    `,
  },
};
