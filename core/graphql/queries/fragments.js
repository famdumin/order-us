import gql from 'graphql-tag';

export const bannerFragments = {
  bannerDetail: gql`
    fragment BannerDetail on Banner {
      id
      name
      url
      image
      webServiceType
      isDel
    }
  `,
};

export const productCategoryFragments = {
  productCategoryDetail: gql`
    fragment ProductCategoryDetail on ProductCategory {
      id
      name
      image
      isDel
      webServiceType
    }
  `,
};

export const webFragments = {
  webDetail: gql`
    fragment WebDetail on Web {
      id
      categories
      image
      description
      url
      currencyUnit
      isDel
    }
  `,
};

export const orderFragments = {
  user: gql`
    fragment OrderUser on OrderUser {
      id
      username
      fullName
      avatar
      email
      phone
      deliveryLocation
      isWholeSale
      createdAt
      updatedAt
      isDel
    }
  `,
  adUser: gql`
    fragment AdOrderUser on AdOrderUser {
      id
      username
      fullName
      avatar
      email
      phone
      deliveryLocation
      isWholeSale
      createdAt
      updatedAt
      isDel
    }
  `,
  staffInfo: gql`
    fragment OrderStaffInfo on OrderStaff {
      customerCare {
        ...OrderUser
      }
      warehouse {
        ...OrderUser
      }
      order {
        ...OrderUser
      }
    }
  `,
  adStaffInfo: gql`
    fragment AdOrderStaffInfo on AdOrderStaff {
      customerCare {
        ...AdOrderUser
      }
      warehouse {
        ...AdOrderUser
      }
    }
  `,
  staff: gql`
    fragment OrderStaff on OrderStaff {
      customerCareStaff
      warehouseStaff
      orderStaff
    }
  `,
  adStaff: gql`
    fragment AdOrderStaff on AdOrderStaff {
      customerCareStaff
      warehouseStaff
    }
  `,
};
