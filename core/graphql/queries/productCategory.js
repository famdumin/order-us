import gql from 'graphql-tag';

import { productCategoryFragments } from './fragments';

export default {
  query: {
    getProductCategories: gql`
      ${productCategoryFragments.productCategoryDetail}
      query($filters: InputPagingRequest) {
        getProductCategories(filters: $filters) {
          success
          message
          result {
            docs {
              ...ProductCategoryDetail
            }
            totalDocs
            limit
            hasPrevPage
            hasNextPage
            page
            totalPages
            offset
            prevPage
            nextPage
            pagingCounter
            meta
          }
        }
      }
    `,
    getProductCategory: gql`
      ${productCategoryFragments.productCategoryDetail}
      query($productCategory: InputProductCategoryGet!) {
        getProductCategory(productCategory: $productCategory) {
          success
          message
          result {
            ...ProductCategoryDetail
          }
        }
      }
    `,
  },
  mutation: {
    createProductCategory: gql`
      ${productCategoryFragments.productCategoryDetail}
      mutation($productCategory: InputProductCategoryCreate!) {
        createProductCategory(productCategory: $productCategory) {
          success
          message
          result {
            ...ProductCategoryDetail
          }
        }
      }
    `,
    updateProductCategory: gql`
      ${productCategoryFragments.productCategoryDetail}
      mutation($productCategory: InputProductCategoryUpdate!) {
        updateProductCategory(productCategory: $productCategory) {
          success
          message
          result {
            ...ProductCategoryDetail
          }
        }
      }
    `,
    deleteProductCategory: gql`
      ${productCategoryFragments.productCategoryDetail}
      mutation($productCategory: InputProductCategoryDeleteRecover!) {
        deleteProductCategory(productCategory: $productCategory) {
          success
          message
          result {
            ...ProductCategoryDetail
          }
        }
      }
    `,
    recoverProductCategory: gql`
      ${productCategoryFragments.productCategoryDetail}
      mutation($productCategory: InputProductCategoryDeleteRecover!) {
        recoverProductCategory(productCategory: $productCategory) {
          success
          message
          result {
            ...ProductCategoryDetail
          }
        }
      }
    `,
  },
};
