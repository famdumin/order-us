import Link from 'next/link';
import Head from 'next/head';

function Error({ statusCode }) {
  return (
    <>
      <Head>
        <title>Error Page</title>
      </Head>
      <div id="notfound">
        <div className="notfound">
          <div className="wrapper">
            <h1 className="title">Oops!</h1>
            <h2 className="subtitle">{`${statusCode} - Our server is on break.`}</h2>
          </div>
          <Link href="/admin/dashboard">
            <a className="btn">Back to Homepage</a>
          </Link>
        </div>
      </div>
      <style jsx>{`
        #notfound {
          position: relative;
          height: 100%;
        }
        .notfound {
          position: absolute;
          left: 50%;
          top: 50%;
          max-width: 520px;
          width: 100%;
          line-height: 1.4;
          text-align: center;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
        }
        .wrapper {
          position: relative;
          height: 200px;
          margin: 0px auto 20px;
          z-index: -1;
        }
        .title {
          font-size: 236px;
          font-weight: 200;
          margin: 0px;
          color: #211b19;
          text-transform: uppercase;
          position: absolute;
          left: 50%;
          top: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
        }
        .subtitle {
          font-size: 28px;
          font-weight: 400;
          text-transform: uppercase;
          color: #211b19;
          background: #fff;
          padding: 10px 5px;
          margin: auto;
          display: inline-block;
          position: absolute;
          bottom: 0px;
          left: 0;
          right: 0;
        }
        .btn {
          display: inline-block;
          font-weight: 700;
          text-decoration: none;
          color: #fff;
          text-transform: uppercase;
          padding: 13px 23px;
          background: #ff6300;
          font-size: 18px;
          -webkit-transition: 0.2s all;
          transition: 0.2s all;
        }
        .btn:hover {
          color: #ff6300;
          background: #211b19;
        }
        @media only screen and (max-width: 767px) {
          .title {
            font-size: 148px;
          }
        }

        @media only screen and (max-width: 480px) {
          .wrapper {
            height: 148px;
            margin: 0px auto 10px;
          }
          .title {
            font-size: 86px;
          }
          .subtitle {
            font-size: 16px;
          }
          .btn {
            padding: 7px 15px;
            font-size: 14px;
          }
      `}</style>
    </>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
