import React, { useCallback } from 'react';
import _ from 'lodash';
import { useQuery, useMutation, useLazyQuery, useApolloClient } from '@apollo/client';
import 'date-fns';
import { useRouter } from 'next/router';

import { initializeApollo } from 'core/apollo';
import { query, mutation } from 'core/graphql/queries';
import { ProductManagement } from 'components/admin/Product';
import { PageTitle, Loading } from 'components/common';
import { toast } from 'utils';
import { PRODUCT_TYPE } from 'constants/product';

const products = [
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
  {
    id: Math.random(),
    image:
      'https://earthsky.org/upl/2018/12/comet-wirtanen-Jack-Fusco-dec-2018-Anza-Borrego-desert-CA-e1544613895713.jpg',
    name: 'Son 3CE Cloud Lip Tint #Immanence',
    type: 'Mỹ phẩm',
    price: 4000000,
  },
];

const tabs = [
  { id: 0, label: 'Sản phẩm phân phối' },
  { id: 1, label: 'Khách hàng liên hệ' },
  { id: 2, label: 'Đối tác và lịch biểu' },
  { id: 3, label: 'Danh mục sản phẩm' },
  { id: 4, label: 'Truy vấn thông tin' },
];

const scheduleTabs = [
  { id: 0, label: 'Đối tác' },
  { id: 1, label: 'Lịch biểu' },
];

const webServiceType = PRODUCT_TYPE.COMMERCE;
const getCaregoriesVariables = (page) => ({
  variables: {
    filters: {
      page,
      query: { webServiceType, isDel: false },
      limit: 5,
    },
  },
});
const getCaregoriesQuery = (page) => ({
  query: query.getProductCategories,
  ...getCaregoriesVariables(page),
});

export default function Product(props) {
  const client = useApolloClient();
  const [tabValue, setTabValue] = React.useState(0);
  const [categories, setCategories] = React.useState([]);
  const [scheduleTabValue, setScheduleTabValue] = React.useState(0);

  const [getCaregories, { loading, data }] = useLazyQuery(query.getProductCategories);
  const [hookAddCategory] = useMutation(mutation.createProductCategory);
  const [hookDeleteCategory] = useMutation(mutation.deleteProductCategory);

  const _handleChangeTab = useCallback((_, newValue) => setTabValue(newValue), [tabValue]);
  const _handleChangeScheduleTab = useCallback((_, newValue) => setScheduleTabValue(newValue), [
    scheduleTabValue,
  ]);

  React.useEffect(() => {
    try {
      if (data && data.getProductCategories) {
        setCategories(data.getProductCategories.result);
      } else if (!data) {
        const getCaregoriesQueryResult = client.readQuery(getCaregoriesQuery(1));
        const categories = getCaregoriesQueryResult.getProductCategories.result;
        setCategories(categories);
      }
    } catch (e) {
      console.error(e.message);
    }
  }, [data]);

  const _handleChangePageCategory = (_, page) => {
    (async function () {
      try {
        const getCaregoriesQueryResult = client.readQuery(getCaregoriesQuery(page));
        const categories = getCaregoriesQueryResult.getProductCategories.result;
        setCategories(categories);
      } catch (e) {
        getCaregories(getCaregoriesVariables(page));
      }
    })();
  };

  const _handleCreateCategory = (data) => {
    const productCategory = { ...data, image: data.image[0], webServiceType };
    hookAddCategory({
      variables: { productCategory },
      update: (cache, mutationResult) => {
        console.log({ cache });
        const { success } = mutationResult.data.createProductCategory;
        if (success) {
          // categoriesRefetch();
          toast.success('Tạo danh mục thành công.');
        }
      },
    }).catch((err) => {
      toast.error(err.graphQLErrors.map((error) => error.message).join('/n'));
    });
  };

  if (loading) return <Loading />;

  return (
    <>
      <PageTitle title="QUẢN LÝ SẢN PHẨM" />
      <ProductManagement
        tabValue={tabValue}
        tabs={tabs}
        onChangeTab={_handleChangeTab}
        scheduleTabValue={scheduleTabValue}
        scheduleTabs={scheduleTabs}
        onChangeScheduleTab={_handleChangeScheduleTab}
        products={products}
        categories={categories}
        onCreateCategory={_handleCreateCategory}
        onChangePageCategory={_handleChangePageCategory}
      />
    </>
  );
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();
  await apolloClient.query(getCaregoriesQuery(1));

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}
