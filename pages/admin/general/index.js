import React from 'react';

import { PageTitle } from 'components/common';
import { InformationHeader, InformationForm } from 'components/admin/General';

const General = () => {
  return (
    <>
      <PageTitle title="THÔNG TIN CHUNG" />
      <InformationHeader />
      <InformationForm />
    </>
  );
};

export default General;
