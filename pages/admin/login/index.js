import React from 'react';
import { useForm } from 'react-hook-form';
import {
  Container,
  Card,
  CardContent,
  Box,
  Typography,
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
import { useMutation } from '@apollo/client';

import { localS, toast } from 'utils';
import { mutation } from 'core/graphql/queries';
import logo from 'assets/images/svg/admin-logo.svg';
import { required, messages } from 'validators';
import { useAuthAdmin } from 'contexts/admin';

const validationSchema = yup.object().shape({
  username: required(messages.requiredField),
  password: required(messages.requiredField),
});

const Login = () => {
  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(validationSchema) });
  const { dispatch } = useAuthAdmin();
  const [hookLogin] = useMutation(mutation.loginAdmin);

  const _handleLogin = (values) => {
    hookLogin({
      variables: { user: values },
      update: (_, mutationResult) => {
        const { success, token } = mutationResult.data.loginAdmin;
        if (success) {
          dispatch({ type: 'LOGIN', payload: mutationResult.data.loginAdmin });
          toast.success('Đăng nhập thành công.');
          if (token) {
            localS.create('admin-token', token);
          }
        }
      },
    }).catch((err) => toast.error(err.graphQLErrors.map((error) => error.message).join('/n')));
  };

  const onSubmit = (values) => _handleLogin(values);

  return (
    <Container component="main" maxWidth="xs">
      <Box mt={12}>
        <Card>
          <CardContent style={{ textAlign: 'center' }}>
            <img src={logo} alt="order us logo" />
            <Box mt={1}>
              <Typography component="h1" variant="h5">
                Authentication
              </Typography>
            </Box>
            <Box mt={1}>
              <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="username"
                  label="Tên đăng nhập"
                  name="username"
                  autoComplete="user"
                  inputRef={register({ required: true, maxLength: 20 })}
                  error={errors.username ? true : false}
                  helperText={errors.username?.message}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="password"
                  label="Mật khấu"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  inputRef={register({ required: true, maxLength: 20 })}
                  error={errors.password ? true : false}
                  helperText={errors.password?.message}
                />
                <FormControlLabel
                  control={<Checkbox color="primary" name="remember_me" inputRef={register} />}
                  label="Nhớ tài khoản"
                  style={{ width: '100%', marginTop: 8 }}
                />
                <Box mt={2} mb={1}>
                  <Button type="submit" fullWidth variant="contained" color="primary" size="large">
                    Login
                  </Button>
                </Box>
              </form>
            </Box>
          </CardContent>
        </Card>
      </Box>
    </Container>
  );
};

export default Login;
