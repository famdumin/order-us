import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { ApolloProvider } from '@apollo/client';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { ToastContainer } from 'react-toastify';

import AdminLayout from 'layouts/admin';
import { AuthAdminProvider } from 'contexts/admin';
import { useApollo } from 'core/apollo';
import { theme } from 'utils/theme';

import 'assets/styles/_main.scss';
import 'react-toastify/dist/ReactToastify.css';

const Layout = ({ children }) => {
  const router = useRouter();
  const isAdmin = router.asPath.includes('admin');
  if (isAdmin) {
    return <AdminLayout>{children}</AdminLayout>;
  }
  return <div>{children}</div>;
};

function MyApp({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState);
  return (
    <>
      <Head>
        <title>Order US</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ApolloProvider client={apolloClient}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <AuthAdminProvider>
            <Layout>
              <Component />
            </Layout>
          </AuthAdminProvider>
        </ThemeProvider>
        <ToastContainer
          newestOnTop
          closeOnClick
          autoClose={2000}
          className="toast-container"
          toastClassName="my-toast"
        />
      </ApolloProvider>
    </>
  );
}

export default MyApp;
