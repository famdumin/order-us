import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { Helmet } from 'react-helmet';
import { ServerStyleSheets } from '@material-ui/core/styles';

export default class CyberDocument extends Document {
  static async getInitialProps(...args) {
    const sheets = new ServerStyleSheets();
    const originalRenderPage = args[0].renderPage;

    args[0].renderPage = () =>
      originalRenderPage({
        enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
      });

    // see https://github.com/nfl/react-helmet#server-usage for more information
    // 'head' was occupied by 'renderPage().head', we cannot use it
    const documentProps = await super.getInitialProps(...args);

    return {
      ...documentProps,
      helmet: Helmet.renderStatic(),
      // styles: [...React.Children.toArray(documentProps.styles), sheets.getStyleElement()]
    };
  }

  // should render on <html>
  get helmetHtmlAttrComponents() {
    return this.props.helmet.htmlAttributes.toComponent();
  }

  // should render on <body>
  get helmetBodyAttrComponents() {
    return this.props.helmet.bodyAttributes.toComponent();
  }

  // should render on <head>
  get helmetHeadComponents() {
    return Object.keys(this.props.helmet)
      .filter((el) => !['htmlAttributes', 'bodyAttributes', 'title'].includes(el))
      .map((el) => this.props.helmet[el].toComponent());
  }

  render() {
    return (
      <html {...this.helmetHtmlAttrComponents}>
        <Head>{this.helmetHeadComponents}</Head>
        <body {...this.helmetBodyAttrComponents}>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
