export const PRODUCT_TYPE = {
  ORDER: 'ORDER',
  COMMERCE: 'COMMERCE',
  ALL: 'ALL',
};

export const PRODUCT_KIND = {
  AD_ORDER: 'adOrder',
  ORDER: 'order',
};

export const PRODUCT_LIST_LIMIT = 12;
