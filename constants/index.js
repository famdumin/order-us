import ChromeLogo from 'assets/icons/chrome-logo.svg';
import iOSLogo from 'assets/icons/ios-logo.svg';
import AndroidLogo from 'assets/icons/android-logo.svg';
import FacebookLogo from 'assets/icons/facebook-logo.svg';
import InstagramLogo from 'assets/icons/instagram.svg';
import TwitterLogo from 'assets/icons/twitter.svg';
import BackgroundForm from 'assets/images/background-popup.svg';
import Amazon from 'assets/images/amazon.svg';
import Alba from 'assets/images/alba.svg';
import Nick from 'assets/images/nick.svg';
import PayImage1 from 'assets/images/pay-1.png';
import PayImage2 from 'assets/images/pay-2.png';
import Walmart from 'assets/images/walmart.svg';
import ReasonIcon1 from 'assets/icons/reason-icon-1.svg';
import ReasonIcon2 from 'assets/icons/reason-icon-2.svg';
import ReasonIcon3 from 'assets/icons/reason-icon-3.svg';
import ReasonIcon4 from 'assets/icons/reason-icon-4.svg';
import ReasonIcon5 from 'assets/icons/reason-icon-5.svg';
import ReasonIcon6 from 'assets/icons/reason-icon-6.svg';
import ReasonIcon7 from 'assets/icons/reason-icon-7.svg';
import VietnamIcon from 'assets/icons/vietnam.svg';
import USAIcon from 'assets/icons/usa-logo.svg';
import TimeIcon from 'assets/icons/time-icon.svg';
import TranslateIcon from 'assets/icons/translate-icon.svg';
import PhoneIcon from 'assets/icons/phone-icon.svg';
import ArrowDownIcon from 'assets/icons/arrow-down-icon.svg';
import HeaderLogo from 'assets/icons/header-logo.svg';
import DownloadIcon from 'assets/icons/download-icon.svg';
import HomepageFieldIcon from 'assets/icons/homepage-field-icon.svg';
import FlashSaleImg1 from 'assets/icons/flash-sale-1.svg';
import FlashSaleImg2 from 'assets/icons/flash-sale-2.svg';
import CartFix from 'assets/icons/cart-fix.svg';
import CartRemove from 'assets/icons/cart-remove.svg';

export const DEFAULT_ACTIONS = {
  READ: 'READ',
  CREATE: 'CREATE',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE',
};

export const VALIDATE_TEMPLATES = {
  REQUIRED: {
    required: 'Trường này bắt buộc điền',
  },
  MIN_VALUE: (length) => ({
    min: {
      value: length,
      message: `Nhỏ nhất là ${length}`,
    },
  }),
  MAX_VALUE: (length) => ({
    max: {
      value: length,
      message: `Lớn nhất là ${length}`,
    },
  }),
  MIN_LENGTH: (length) => ({
    maxLength: {
      value: length,
      message: `Tối thiếu ${length} ký tự`,
    },
  }),
  MAX_LENGTH: (length) => ({
    maxLength: {
      value: length,
      message: `Tối đa ${length} ký tự`,
    },
  }),
  REGEX: (pattern, format) => ({
    pattern: {
      value: pattern,
      message: `Sai định dạng cho phép: ${format}`,
    },
  }),
  CUSTOM: (customValidateObject) => ({
    validate: customValidateObject,
  }),
};

export const DEFAULT_FORMAT = {
  DATE_SERVER: 'YYYY-MM-DD',
  DATE: 'DD-MM-YYYY',
  TIME: 'HH:mm',
};

export const PAGINATION = {
  DEFAULT_PAGE: 0,
  DEFAULT_ROW_PER_PAGE: 10,
  ROWS_PER_PAGE_OPTIONS: [10, 25, 50, { label: 'All', value: -1 }],
};

export const HEADER_BODY = {
  icon: {
    logo: HeaderLogo,
    downloadIcon: DownloadIcon,
  },
  dropdown: {
    website: 'Chọn trang web',
    product: 'Chọn mặt hàng',
  },
  dowloadTitle: 'Download Extension để sử dụng dịch vụ',
};

export const HEADER_TOP = {
  price: { title: 'Tỷ giá Mỹ: 1$ = 25.000VNĐ', icon: USAIcon },
  time: { title: 'Thứ hai đến thứ 7 8:00 - 17:00', icon: TimeIcon },
  phoneNumber: { title: 'Đường dây nóng: 096 988 1028', icon: PhoneIcon },
  language: { title: 'Chọn ngôn ngữ', leftIcon: TranslateIcon, rightIcon: ArrowDownIcon },
};

export const HOMEPAGE_ITEM_TITLE = {
  rightText: 'Xem tất cả',
  icon: HomepageFieldIcon,
};

export const FOOTER_DATA = {
  FOOTER_LOGO: HeaderLogo,
  INFO: {
    label: 'Order Mỹ',
    content: [
      {
        label: 'Địa chỉ',
        content: [
          { site: 'Việt Nam', value: 'A5, Nam Đồng, Đống Đa, Hà Nội' },
          {
            site: 'Hoa Kỳ',
            value: '212-258 Vermont St, Brooklyn, New York, Hoa Kỳ',
          },
        ],
      },
      {
        label: 'Số điên thoại',
        content: [
          { site: 'Việt Nam', value: '096 988 1028' },
          { site: 'Hoa Kỳ', value: '01029429129' },
        ],
      },
      {
        label: 'Email',
        content: [
          { site: 'Việt Nam', value: 'business@orderus.com' },
          { site: 'Hoa Kỳ', value: 'va@orderus.com' },
        ],
      },
    ],
  },
  TIME: { label: 'Giờ làm việc', value: 'Thứ hai đến thứ 7 8:00 - 17:00' },
  LINK: {
    label: 'Liên kết',
    content: [
      'Web đặt hàng',
      'Bảng báo giá ',
      'Hướng dẫn mua hàng',
      'Hình thức thanh toán',
      'Chính sách bảo mật thông tin',
      'Chính sách chung',
    ],
  },
  SUPPORT: {
    label: 'Hỗ trợ mua hàng',
    content: [
      { icon: ChromeLogo, label: 'Chrome Extension' },
      { icon: iOSLogo, label: 'Mobile App - IOS' },
      { icon: AndroidLogo, label: 'Mobile App - Android' },
    ],
  },
  BOTTOM_LOGO: [FacebookLogo, InstagramLogo, TwitterLogo],
};

export const POP_UP_DATA = {
  TITLE_LOGIN: 'Đăng nhập',
  TITLE_REGISTER: 'Tạo tài khoản',
  BUTTON_LOGIN: 'Đăng nhập',
  BUTTON_REGISTER: 'Đăng ký',
  BUTTON_LOGOUT: 'Đăng xuất',
  USERNAME: 'Tên đăng nhập',
  PASSWORD: 'Mật khẩu',
  OLD_PASSWORD: 'Mật khẩu cũ',
  NEW_PASSWORD: 'Mật khẩu mới',
  CONFIRM_PASSWORD: 'Xác nhận mật khẩu mới',
  FORGET_PASSWORD: 'Quên mật khẩu ?',
  REMEMBER_PASSWORD: 'Nhớ tài khoản',
  DONT_HAVE_ACCOUNT: 'Bạn không có tài khoản?',
  HAVE_ACCOUNT: 'Bạn đã có tài khoản?',
  BACKGROUNG_IMAGE: BackgroundForm,
  FULL_NAME: 'Tên đầy đủ',
  EMAIL: 'Email',
  PHONE: 'Số điện thoại',
  ADDRESS: 'Địa chỉ',
};

export const AUTH_MODE = {
  LOGIN: 'Đăng nhập',
  REGISTER: 'Đăng ký',
};

export const HOMEPAGE_CONTENT = {
  FLASH_SALE: [FlashSaleImg1, FlashSaleImg2],
  ORTHER_WEB: { label: 'Những trang web order tại mỹ', images: [Amazon, Walmart, Alba, Nick] },
  REASON: {
    label: 'Tại sao phải chọn order Mỹ',
    content: [
      { title: 'Mua hàng tại những trang web nổi tiếng nhất Mỹ', icon: ReasonIcon1 },
      { title: 'Tư vấn mua hàng để được giá tốt nhất', icon: ReasonIcon2 },
      {
        title: 'Trải nghiệm mua hàng dễ dàng tiện lợi qua ứng dụng của order Mỹ',
        icon: ReasonIcon3,
      },

      {
        title: 'Phí dịch vụ hấp dẫn cho quý khách hàng mua lẻ và mua sỉ',
        icon: ReasonIcon4,
      },
      { title: 'Thời gian vận chuyển nhanh chóng, tiện lợi', icon: ReasonIcon5 },

      {
        title: 'Cung cấp thông tin Sale, khuyến mãi giảm giá liên tục',
        icon: ReasonIcon6,
      },
      { title: 'Cam kết hoàn tiền 100% nếu mất hoặc hỏng hàng', icon: ReasonIcon7 },
    ],
  },
  SUPPORT: {
    label: 'Hỗ trợ trực tuyến',
    content: [
      {
        title: 'Hỗ trợ kinh doanh',
        content: {
          vietnam: {
            label: 'Đại diện tại Việt Nam',
            phoneNumber: '0969881028',
            icon: VietnamIcon,
          },
          usa: {
            label: 'Đại diện tại Mỹ',
            phoneNumber: '202-555-0128',
            icon: USAIcon,
          },
        },
      },
      {
        title: 'Chăm sóc khách hàng',
        content: {
          vietnam: {
            label: 'Đại diện tại Việt Nam',
            phoneNumber: '0969881028',
            icon: VietnamIcon,
          },
          usa: {
            label: 'Đại diện tại Mỹ',
            phoneNumber: '202-555-0128',
            icon: USAIcon,
          },
        },
      },
    ],
  },
  EXPERIENCE: {
    label: 'Kinh nghiệm mua hàng',
    content: [
      {
        title: 'Kinh nghiệm đặt hàng',
        posts: [
          'Order hàng hàn quốc một cách nhanh chóng tiện lợi',
          'Đi du lịch Hàn Quốc mùa tuyết trắng',
          'Mùa thu đi ngắm lá đỏ tại Seoraksan',
          'Mua sắm ở Myeongdong',
          'BTS thống trị Billboard khi vừa khép lại World Tour',
        ],
      },
      {
        title: 'Tin tức mới nhất',
        posts: [
          'Order hàng hàn quốc một cách nhanh chóng tiện lợi',
          'Đi du lịch Hàn Quốc mùa tuyết trắng',
          'Mùa thu đi ngắm lá đỏ tại Seoraksan',
          'Mua sắm ở Myeongdong',
          'BTS thống trị Billboard khi vừa khép lại World Tour',
        ],
      },
    ],
  },
};

export const productConstants = {
  searchModes: {
    NAME: 'SEARCH_BY_NAME',
    PARTNER: 'SEARCH_BY_PARTNER',
  },
  formModes: {
    CREATE: 'create',
    EDIT: 'edit',
  },
  schedulerType: {
    PERCENT: 'percent',
    UNIT: 'unit',
  },
};

export const ORDER_STATUSES = [
  { label: 'Chờ thanh toán', value: 'Chờ thanh toán' },
  { label: 'Đã thanh toán', value: 'Đã thanh toán' },
  { label: 'Đang đặt hàng', value: 'Đang đặt hàng' },
  { label: 'Đặt hàng thất bại', value: 'Đặt hàng thất bại' },
  { label: 'Đã nhập kho', value: 'Đã nhập kho' },
  { label: 'Đã gửi hàng', value: 'Đã gửi hàng' },
  { label: 'Đã nhận hàng', value: 'Đã nhận hàng' },
  { label: 'Đã huỷ đơn', value: 'Đã huỷ đơn' },
  { label: 'Đang khiếu nại', value: 'Đang khiếu nại' },
];

export const USER_ORDER_STATUSES = {
  IN_CART: 'Giỏ hàng',
  BOOKING: 'Chờ thanh toán',
  PAID: 'Đã thanh toán',
  ORDERING: 'Đang đặt hàng',
  ORDER_FAILED: 'Đặt hàng thất bại',
  AT_WARE_HOUSE: 'Đã nhập kho',
  // WAIT_WEIGHT_FEE_PAYMENT: 'Chờ thanh toán khối lượng',
  // PAID_WEIGHT_FEE_PAYMENT: 'Đã thanh toán khối lượng',
  ON_DELIVERY: 'Đã gửi hàng',
  RECEIVED: 'Đã nhận hàng',
  CANCELED: 'Đã huỷ đơn',
  COMPLAINING: 'Đang khiếu nại',
  // REFUNDING: 'Đang hoàn tiền',
  // REFUNDED: 'Đã hoàn tiền'
};

export const TRANSACTION_TYPES = [
  { label: 'Nạp tiền', value: 'Nạp tiền' },
  { label: 'Thanh toán đơn hàng', value: 'Thanh toán đơn hàng' },
  { label: 'Thanh toán khối lượng', value: 'Thanh toán khối lượng' },
  { label: 'Hoàn tiền', value: 'Hoàn tiền' },
  { label: 'Quản trị viên thay đổi', value: 'Quản trị viên thay đổi' },
];

export const COMMERCE_TYPE = {
  TRADING: 'TRADING',
  ADVERTISEMENT: 'ADVERTISEMENT',
};

export const CURRENCY_UNIT = {
  WON: 'WON',
  USD: 'USD',
  VND: 'VND',
};

export const ORDER_STATUS_COLORS = {
  ['Giỏ hàng']: { background: '#ffa726', text: '#fafafa' }, // Orange
  ['Chờ thanh toán']: { background: '#ffa726', text: '#fafafa' }, // Orange
  ['Đã thanh toán']: { background: '#4caf50', text: '#fafafa' }, // Green
  ['Đang đặt hàng']: { background: '#009688', text: '#fafafa' }, // Teal
  ['Đặt hàng thất bại']: { background: '#212121', text: '#fafafa' }, // Grey
  ['Đã nhập kho']: { background: '#3f51b5', text: '#fafafa' }, //Indigo
  ['Đã gửi hàng']: { background: '#90a4ae', text: '#000000' }, // Blue Grey
  ['Đã nhận hàng']: { background: '#546e7a', text: '#fafafa' }, // Blue Grey Darker
  ['Đã huỷ đơn']: { background: '#263238', text: '#fafafa' }, // Black
  ['Đang khiếu nại']: { background: '#ef5350', text: '#000000' }, // Red
};

export const PRICE_BOARD = {
  TITLE: 'Bảng báo giá',
  RECIPE: {
    title: 'Công thức tính giá',
    recipe:
      'GIÁ WEB (Giá hiển thị trên website) * TỈ GIÁ + PHÍ ĐƠN + PHÍ SHIP HÀN QUỐC + PHÍ SHIP VỀ VIỆT NAM + PHÍ SHIP NỘI ĐỊA (NẾU CÓ) + PHỤ PHÍ (NẾU CÓ)',
    content: [
      'Với những đơn hàng > 1(kg), Order Mỹ sẽ miễn phí ship nội địa ',
      'Với những đơn hàng < 1kg, Order Mỹ sẽ gom chung đơn để giảm chi phí vận chuyển. Khách hàng sẽ chịu thêm khoản phí nội địa theo giá của các công ty vận chuyển tại Việt Nam',
    ],
  },
  PRUCHASE_FEE: {
    title: 'Phí mua hàng',
    content: [
      'Đơn hàng có giá trị từ 0 đ trở lên',
      'Phí mua hàng: 6% tổng giá trị sản phẩm đã mua trong đơn hàng.',
      'Đơn hàng có giá trị từ 50,000,000 đ trở lên',
      'Phí mua hàng: 4% tổng giá trị sản phẩm đã mua trong đơn hàng.',
    ],
    special: 'Đối với khách hàng mua sỉ',
    specialContent: [
      'Đơn hàng có giá trị từ 0 đ trở lên',
      'Phí mua hàng: 3% tổng giá trị sản phẩm đã mua trong đơn hàng.',
      'Đơn hàng có giá trị từ 50,000,000 đ trở lên',
      'Phí mua hàng: 2% tổng giá trị sản phẩm đã mua trong đơn hàng.',
    ],
  },
  USA_SHIPPING_FEE: {
    title: 'Phí ship hàng nội địa Mỹ',
    content: ['Phí vận chuyển hàng nội địa trên mỗi website: 60,000 đ'],
    special: 'Đối với khách hàng mua sỉ',
    specialContent: ['Phí vận chuyển hàng nội địa trên mỗi website: 60,000 đ'],
  },
  VN_SHIPPING_FEE: {
    title: 'Phí ship hàng về Việt Nam',
    content: ['Đơn hàng có khối lượng từ 1 Kg', 'Phí vận chuyển về nước: 200,000 đ'],
    special: 'Đối với khách hàng mua sỉ',
    specialContent: ['Đơn hàng có khối lượng từ 1 Kg', 'Phí vận chuyển về nước: 200,000 đ'],
  },
  EXTRA_FEE: {
    title: 'Phụ phí',
    content: [' Đơn hàng có khối lượng từ 0 Kg trở lên', 'Phí vận chuyển về nước: 0 đ'],
  },
};

export const PAY = {
  IMAGES: {
    image_1: PayImage1,
    image_2: PayImage2,
  },
  TITLE: 'Hình thức thanh toán',
  RECIPE: {
    title: 'Công thức tính giá',
    recipe:
      'GIÁ WEB (Giá hiển thị trên website) * TỈ GIÁ + PHÍ ĐƠN + PHÍ SHIP HÀN QUỐC + PHÍ SHIP VỀ VIỆT NAM + PHÍ SHIP NỘI ĐỊA (NẾU CÓ) + PHỤ PHÍ (NẾU CÓ)',
    content: [
      'Với những đơn hàng > 1(kg), Order Mỹ sẽ miễn phí ship nội địa ',
      'Với những đơn hàng < 1kg, Order Mỹ sẽ gom chung đơn để giảm chi phí vận chuyển. Khách hàng sẽ chịu thêm khoản phí nội địa theo giá của các công ty vận chuyển tại Việt Nam',
    ],
  },
  NOTE: [
    'Áp dụng cho khách hàng đã có tài khoản tại Order Hàn Quốc.',
    'Nếu chưa, quý khách vui lòng tạo tài khoản tại đây',
  ],
  STEP_ONE: {
    title: 'Bước 1',
    content: [
      'Bấm vào Giỏ hàng để nhìn thấy các đơn hàng mình đã mua',
      'Các đơn hàng từ các trang web mà OrderUS liên kết',
      'Các đơn hàng từ mua trực tiếp trên trang OrderUS',
    ],
  },
  STEP_TWO: {
    title: 'Bước 2',
    content: [
      'Sau khi bấm nút thanh toán, các bạn có thể chuyển tiền vào tài khoản được hiển thị để thanh toán đơn hàng đã mua',
    ],
  },
};

export const RECLAMATION = {
  TITLE: 'Khiếu nại',
  NOTE: {
    title: 'Lưu ý',
    content: [
      'Order Hàn Quốc nhận khiếu nại về sản phẩm trong vòng 48h kể từ khi nhận hàng.',
      'Thời gian phản hồi & xử lý khiếu nại: Trong vòng 2 ngày kể từ khi nhận được khiếu nại của khách hàng.',
    ],
  },
  TIME_FACTOR: {
    title: 'Thời gian xử lý khiếu nại có thể thay đổi phụ thuộc vào các yếu tố sau',
    content: [
      'Đối với khiếu nại liên quan đến bên thứ 3 (bên vận chuyển, website đặt hàng…), Order Hàn Quốc sẽ làm việc với các đơn vị để có thông tin xử lý khiếu nại.',
      'Order Hàn Quốc sẽ không xử lý các khiếu nại khi sản phẩm đã được quý khách hàng sử dụng, hoặc thay đổi so với nguyên trạng ban đầu.',
    ],
  },
  METHOD: {
    title: 'Cách thức khiếu nại',
    content: [
      'Quý khách hàng có thể liên hệ trực tiếp với Nhân viên CSKH của đơn hàng khiếu nại. Hoặc inbox trực tiếp qua Fanpage của Order Hàn Quốc tại đây',
    ],
    button: ['0969.881.028', 'Fanpage'],
  },
  COMPENSATION: {
    title: 'Quy định bồi thường',
    content: [
      'Trường hợp hết hàng hoặc không đặt được hàng của website, tổng số tiền hoá đơn sẽ được tính lại và sẽ chuyển khoản lại cho quý khách hàng sau khi tính toán hết các chi phí vận chuyển nếu tiền chuyển khoản ban đầu còn thừa.',
      'Trường hợp: Sai màu, sai kích cỡ, sai mẫu mức độ nhẹ (ví dụ hình ảnh trên áo quần khác mô tả, đặt dài tay về cộc tay…). Thậm chí nếu khách không ưng ý với sản phẩm, Order Hàn Quốc sẽ hỗ trợ khách hàng liên hệ với nhà cung cấp (NCC) để đổi hàng, phí đổi trả do khách hàng chịu.',
      'Trường hợp hàng hỏng do quá trình vận chuyển , Order Hàn Quốc sẽ bán giúp cho quý khách hàng hoặc bồi thường 100% tiền hàng (không bao gồm phí vận chuyển, phí dịch vụ).',
      'Trường hợp hàng về muộn quá 30 ngày kể từ thời điểm mua hàng Order Hàn Quốc sẽ huỷ đơn hàng và hoàn lại tiền cho quý khách.',
    ],
  },
  BUTTON: ['Chính sách vận chuyển', 'Chính sách đổi trả', 'Danh mục hàng cấm nhập'],
};

export const USER_INFO = {
  TITLE: {
    UPDATE_INFO: 'Sửa thông tin',
    CHANGE_PASSWORD: 'Đổi mật khẩu',
  },
  NOTE: {
    title: 'Đăng ký thành viên mua sỉ',
    content: [
      'Thành viên mua sỉ phải dùng tối thiểu 20.000.000đ mỗi tháng. Bạn sẽ hưởng giảm giá ưu đãi theo giá sỉ cho các đơn hàng (xem tại đây). Tài khoản sẽ tự động được khôi phục về tài khoản thường nếu không dùng đủ tiêu chí trong tháng !',
      'Tài khoản dùng đủ 20.000.000đ trong 30 ngày kể từ ngày đăng ký sẽ được tự động chuyển sang tài khoản mua sỉ.',
    ],
  },
};

export const POLICY = {
  TITLE: 'Các chính sách',
  CONTENT: [
    { id: 0, title: 'Khiếu nại' },
    { id: 1, title: 'Đổi trả hàng' },
    { id: 2, title: 'Bảo mật thông tin' },
    { id: 3, title: 'Vận chuyển' },
    { id: 4, title: 'Danh mục hàng cấm nhập khẩu' },
    { id: 5, title: 'Chính sách chung' },
    { id: 6, title: 'Thanh toán' },
    { id: 7, title: 'Báo giá' },
  ],
};

export const CART_DATA = {
  TABLE_HEADER: {
    product: 'Sản phẩm',
    note: 'Ghi chú',
    price: 'Đơn giá',
    count: 'Số lượng',
    total: 'Tổng tiền',
  },
  TABLE_ITEM: {
    color: 'Màu sắc',
    weight: 'Cân nặng',
    insurrance: 'Bảo hiểm',
    fix: { title: 'Sửa', icon: CartFix },
    remove: { title: 'Xoá', icon: CartRemove },
  },
  RECEIPT: {
    TITLE: 'Hoá đơn',
    PRICE: 'Giá sãn phẩm',
    FEE: 'Phí mua hàng',
    INSURRANCE: 'Bảo hiểm',
    TOTAL: 'Tổng cộng',
    VN_SHIPPING_FEE: 'Phí ship về VN dự kiến',
    TYPE: 'Hình thức',
    ADDRESS: 'Địa chỉ nhận hàng',
    PAYMENT: 'Thanh toán',
    NOTE: [
      '(*) bao gồm phí mua, vận chuyển trong nước Mỹ, chi phí khác.',
      'Thời gian về Việt Nam dự kiến 14 ngày.',
    ],
  },
};
