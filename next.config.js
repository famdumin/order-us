// const withImages = require('next-images');
// const withSass = require('@zeit/next-sass');
// const withPlugins = require('next-compose-plugins');

// module.exports = withPlugins(
//   [
//     [
//       withSass,
//       {
//         cssModules: true,
//         cssLoaderOptions: {
//           importLoaders: 1,
//           localIdentName: '[local]___[hash:base64:5]',
//         },
//       },
//     ],
//     withImages,
//   ],
//   (nextConfig = {}) => {
//     return Object.assign({}, nextConfig, {
//       webpack(config, options) {
//         config.module.rules.push({
//           enforce: 'pre',
//           test: /.scss$/,
//           loader: 'sass-resources-loader',
//           options: {
//             resources: ['assets/styles/_main.scss'],
//           },
//         });
//         return config;
//       },
//     });
//   }
// );

require('dotenv').config();
const withImages = require('next-images');
const withStyles = require('@webdeb/next-styles');
const withFonts = require('nextjs-fonts');
const webpack = require('webpack');

module.exports = withImages(
  withStyles(
    withFonts({
      less: true,
      sass: true,
      modules: true,
      lessLoaderOptions: {
        javascriptEnabled: true,
      },
      webpack: (config, options) => {
        config.plugins.push(new webpack.EnvironmentPlugin(process.env));
        return config;
      },
      publicRuntimeConfig: {
        localeSubpaths: {
          vi: 'vi',
          kr: 'kr',
        },
      },
    })
  )
);
