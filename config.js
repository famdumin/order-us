const config = {
  IS_DEV: process.env.NODE_ENV
    ? process.env.NODE_ENV === 'development' || process.env.ENV === 'staging'
    : true,
  IS_DEV_ONLINE: process.env.MODE === 'ONLINE',
  PORT: process.env.PORT || 7000,
  GRAPHQL_ENDPOINT: process.env.GRAPHQL_ENDPOINT || '/graphql',
  RESTAPI_ENDPOINT: process.env.RESTAPI_ENDPOINT || '/api',
};

config.API_PORT =
  process.env.API_PORT || (config.IS_DEV && !config.IS_DEV_ONLINE) ? `:${7001}` : '';

config.WEB_PROTOCOL = process.env.WEB_PROTOCOL || `http${!config.IS_DEV ? 's' : ''}`;

config.SOCKET_PROTOCOL = process.env.SOCKET_PROTOCOL || `ws${!config.IS_DEV ? 's' : ''}`;

config.STYLE_NAME =
  process.env.STYLE_NAME || config.IS_DEV
    ? config.IS_DEV_ONLINE
      ? 'orderusstag'
      : 'orderusdev'
    : 'orderusprod';

config.API_HOST_NAME =
  process.env.API_HOST_NAME ||
  (config.IS_DEV
    ? process.env.ENV === 'staging' || config.IS_DEV_ONLINE
      ? '139.180.186.224'
      : 'localhost'
    : 'api-production-here');

config.RESTAPI_HTTP_URI =
  process.env.RESTAPI_HTTP_URI ||
  `${config.WEB_PROTOCOL}://${config.API_HOST_NAME}${config.API_PORT}${config.RESTAPI_ENDPOINT}`;

config.GRAPHQL_HTTP_URI =
  process.env.GRAPHQL_HTTP_URI ||
  `${config.WEB_PROTOCOL}://${config.API_HOST_NAME}${config.API_PORT}${config.GRAPHQL_ENDPOINT}`;

config.GRAPHQL_WS_URI =
  process.env.GRAPHQL_WS_URI ||
  `${config.SOCKET_PROTOCOL}://${config.API_HOST_NAME}${config.API_PORT}${config.GRAPHQL_ENDPOINT}`;

config.IMAGE_HOST =
  process.env.IMAGE_HOST || `${config.WEB_PROTOCOL}://${config.API_HOST_NAME}${config.API_PORT}/`;

module.exports = config;
