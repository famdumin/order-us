import React, { useEffect, useContext, useReducer } from 'react';
import { useQuery } from '@apollo/client';
import { useRouter } from 'next/router';

import { Loading } from 'components/common';
import { localS, toast } from 'utils';
import { query } from 'core/graphql/queries';

const AuthAdminContext = React.createContext();
const initialState = {
  auth: null,
  isLoggedIn: false,
  isLoading: true,
};

function authReducer(state, action) {
  switch (action.type) {
    case 'LOGIN':
      return Object.assign({}, state, { isLoggedIn: true, auth: action.payload });
    case 'LOGOUT':
      return Object.assign({}, state, { isLoggedIn: false, auth: null });
    case 'LOADED':
      return Object.assign({}, state, { isLoading: false });
    default:
      return state;
  }
}

export const AuthAdminProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const router = useRouter();
  const isLoginPage = router.route.includes('admin/login');

  const { loading, error, data } = useQuery(query.checkAuthAdmin, {
    variables: {
      ...(typeof window !== 'undefined' && {
        token: { token: localS.read('admin-token') },
      }),
    },
  });

  useEffect(() => {
    if (data && data.checkAuthAdmin) {
      const auth = data.checkAuthAdmin;
      if (auth.success) {
        dispatch({ type: 'LOGIN', payload: auth });
      } else {
        router.push('/admin/login');
      }
      //handle enter login on browser
      setTimeout(() => {
        dispatch({ type: 'LOADED' });
      }, 500);
    }
  }, [data]);

  useEffect(() => {
    //handle back to login page
    if (state.isLoggedIn && isLoginPage) {
      router.push('/admin/product');
    }
  }, [state.isLoggedIn, isLoginPage]);

  if (loading || state.isLoading) {
    return <Loading />;
  }

  if (error) {
    toast.error(error.message);
    return null;
  }

  return (
    <AuthAdminContext.Provider value={{ state, dispatch }}>{children}</AuthAdminContext.Provider>
  );
};

export const useAuthAdmin = () => useContext(AuthAdminContext);
