import React from 'react';

import { useStyles } from './MainContent.styled';

const MainContent = ({ children }) => {
  const classes = useStyles();

  return (
    <main className={classes.wrapper}>
      <div className={classes.toolbar} />
      <div className={classes.content}>{children}</div>
    </main>
  );
};

export default MainContent;
