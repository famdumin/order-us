import React from 'react';
import clsx from 'clsx';
import { AppBar, Toolbar, IconButton, Menu, MenuItem } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useMutation } from '@apollo/client';
import { useRouter } from 'next/router';

import { localS, toast } from 'utils';
import { mutation } from 'core/graphql/queries';
import { useAuthAdmin } from 'contexts/admin';
import { useStyles } from './NavBar.styled';

const NavBar = ({ drawerOpen, onClick, drawerWidth }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { dispatch } = useAuthAdmin();
  const router = useRouter();
  const [hookLogout] = useMutation(mutation.logout);

  const classes = useStyles({ drawerWidth });

  const handleMenu = (event) => setAnchorEl(event.currentTarget);
  const handleClose = () => setAnchorEl(null);
  const handleLogout = () =>
    hookLogout({
      update: (_, mutationResult) => {
        if (mutationResult.data.logout && mutationResult.data.logout.success === true) {
          dispatch({ type: 'LOGOUT' });
          localS.delete('admin-token');
          toast.success('Đăng xuất thành công.');
          router.push('/admin/login');
        }
      },
    }).catch((err) => toast.success(err.graphQLErrors.map((error) => error.message).join('/n')));

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: drawerOpen,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="default"
          aria-label="open drawer"
          onClick={onClick}
          edge="start"
          className={classes.menuButton}
        >
          {drawerOpen ? <MenuOpenIcon /> : <MenuIcon />}
        </IconButton>
        <div>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="primary"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={handleLogout}>Thoát</MenuItem>
          </Menu>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
