import React, { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

import { useAuthAdmin } from 'contexts/admin';
import { Loading } from 'components/common';
import Navbar from '../NavBar';
import Sidebar from '../SideBar';
import MainContent from '../MainContent';
import { useStyles } from './AdminLayout.styled';

const drawerWidth = 240;

const AuthLayout = ({ children }) => {
  const [open, setOpen] = useState(true);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Navbar drawerOpen={open} drawerWidth={drawerWidth} onClick={() => setOpen(!open)} />
      <Sidebar drawerOpen={open} drawerWidth={drawerWidth} />
      <MainContent style={classes.toolbar}>{children}</MainContent>
    </div>
  );
};

const AdminLayout = React.memo(({ children, title = 'Order Us' }) => {
  const { state } = useAuthAdmin();
  const { isLoggedIn } = state;
  const { route } = useRouter();
  const isAdminLogin = route.includes('admin/login');

  const isLoading =
    (!isLoggedIn && !isAdminLogin) || (isLoggedIn && isAdminLogin) || state.isLoading;

  if (isLoading) {
    return <Loading />;
  }

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      {isLoggedIn ? <AuthLayout>{children}</AuthLayout> : children}
    </>
  );
});

export default AdminLayout;
