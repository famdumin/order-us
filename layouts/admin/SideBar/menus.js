import {
  DashboardIcon,
  InformationIcon,
  TeamIcon,
  ContentIcon,
  FeeIcon,
  OrderIcon,
  ProductIcon,
} from './Icons';

export default [
  {
    Icon: DashboardIcon,
    name: 'Bảng điều khiển',
    href: '/admin/dashboard',
  },
  {
    Icon: InformationIcon,
    name: 'Thông tin chung',
    href: '/admin/general',
  },
  {
    Icon: TeamIcon,
    name: 'Quản lý người dùng',
    href: '/admin/user',
  },
  {
    Icon: ContentIcon,
    name: 'Quản lý nội dung',
    href: '/admin/content',
  },
  {
    Icon: FeeIcon,
    name: 'Quản lý phí',
    href: '/admin/fee',
  },
  {
    Icon: OrderIcon,
    name: 'Quản lý đơn hàng',
    href: '/admin/order',
  },
  {
    Icon: ProductIcon,
    name: 'Quản lý sản phẩm',
    href: '/admin/product',
  },
];
