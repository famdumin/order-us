import React from 'react';
import clsx from 'clsx';
import Link from 'next/link';
import { useTheme } from '@material-ui/styles';
import { useRouter } from 'next/router';
import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

import logo from 'assets/images/svg/admin-logo.svg';
import { useStyles } from './SideBar.styled';
import menus from './menus';

const ActiveLink = ({ menu, drawerOpen }) => {
  const { asPath } = useRouter();
  const theme = useTheme();
  const classes = useStyles({ drawerOpen });
  const { href } = menu;
  const isActiveLink = asPath === href;

  return (
    <Link href={href}>
      <ListItem button selected={isActiveLink}>
        <ListItemIcon className={classes.icon}>
          {
            <menu.Icon
              fill={isActiveLink ? theme.palette.secondary.main : theme.palette.primary.main}
            />
          }
        </ListItemIcon>
        <ListItemText primary={menu.name} className={clsx({ [classes.active]: isActiveLink })} />
      </ListItem>
    </Link>
  );
};

const SideBar = (props) => {
  const { drawerOpen } = props;
  const classes = useStyles(props);

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: drawerOpen,
        [classes.drawerClose]: !drawerOpen,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: drawerOpen,
          [classes.drawerClose]: !drawerOpen,
        }),
      }}
    >
      <div
        className={clsx(classes.toolbar, {
          [classes.logoShift]: !drawerOpen,
        })}
      >
        <img src={logo} />
      </div>
      <List className={classes.menus}>
        {menus.map((menu, index) => (
          <ActiveLink menu={menu} key={index.toString()} drawerOpen={drawerOpen} />
        ))}
      </List>
    </Drawer>
  );
};

export default SideBar;
