import React from 'react';
import { Typography } from '@material-ui/core';

const InformationHeader = (props) => (
  <Typography variant="body1" color="textPrimary" gutterBottom>
    Chỉnh sửa thông tin chung cho trang web chính
  </Typography>
);

export default InformationHeader;
