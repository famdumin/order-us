import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    '& .MuiInputLabel-shrink': {
      transform: `translate(0, 1.5px) scale(0.9)`,
      color: theme.palette.primary.main,
    },
    '& .MuiButton-root': {
      marginTop: 25,
    },
  },
}));
