import React from 'react';
import _ from 'lodash';
import {
  Grid,
  TextField,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
  Button,
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';

import { useStyles } from './InformationForm.styled';

const days = [
  { value: 'monday', label: 'Thứ hai' },
  { value: 'tuesday', label: 'Thứ ba' },
  { value: 'wednesday', label: 'Thứ tư' },
  { value: 'thursday', label: 'Thứ năm' },
  { value: 'friday', label: 'Thứ sáu' },
  { value: 'saturday', label: 'Thứ bảy' },
  { value: 'sunday', label: 'Chủ nhật' },
];

const InformationForm = (props) => {
  const classes = useStyles();
  return (
    <form className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <TextField
            required
            margin="normal"
            type="number"
            id="exchange-rate"
            label="Tỷ giá Mỹ"
            fullWidth
            InputLabelProps={{ shrink: true }}
            helperText="Error cho vào đây"
          />
          <TextField
            required
            margin="normal"
            type="time"
            id="open-time"
            fullWidth
            InputLabelProps={{ shrink: true }}
            label="Giờ mở cửa"
            helperText="Error cho vào đây"
          />
          <TextField
            required
            margin="normal"
            type="time"
            id="close-time"
            fullWidth
            InputLabelProps={{ shrink: true }}
            label="Giờ đóng cửa"
            helperText="Error cho vào đây"
          />
          <FormControl fullWidth margin="normal">
            <InputLabel id="start-day-label" shrink={true} required>
              Ngày bắt đầu của tuần
            </InputLabel>
            <Select labelId="start-day-label" id="start-day" defaultValue="">
              {_.map(days, (day, index) => (
                <MenuItem value={day.value} key={index.toString()}>
                  {day.label}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>Some important helper text</FormHelperText>
          </FormControl>
          <FormControl fullWidth margin="normal">
            <InputLabel id="end-day-label" shrink={true} required>
              Ngày kết thúc của tuần
            </InputLabel>
            <Select labelId="end-day-label" id="end-day" defaultValue="">
              {_.map(days, (day, index) => (
                <MenuItem value={day.value} key={index.toString()}>
                  {day.label}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>Some important helper text</FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <TextField
            required
            margin="normal"
            id="phone"
            label="Số điện thoại"
            fullWidth
            InputLabelProps={{ shrink: true }}
            helperText="Error cho vào đây"
          />
          <TextField
            required
            margin="normal"
            id="address"
            label="Địa chỉ"
            fullWidth
            InputLabelProps={{ shrink: true }}
            helperText="Error cho vào đây"
          />
          <TextField
            required
            margin="normal"
            id="email"
            label="Email"
            type="email"
            fullWidth
            InputLabelProps={{ shrink: true }}
            helperText="Error cho vào đây"
          />
          <TextField
            required
            margin="normal"
            id="admin-email"
            label="Email nhận thông báo của admin"
            type="email"
            fullWidth
            InputLabelProps={{ shrink: true }}
            helperText="Error cho vào đây"
          />
          <Button
            startIcon={<SaveIcon />}
            variant="contained"
            color="primary"
            size="large"
            fullWidth
          >
            Lưu
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default InformationForm;
