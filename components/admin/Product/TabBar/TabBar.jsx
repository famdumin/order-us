import React from 'react';
import _ from 'lodash';
import { Tabs, Tab, Divider } from '@material-ui/core';

function TabBar({ tabs, value, onChangeTab }) {
  return (
    <>
      <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={onChangeTab}
        aria-label="disabled tabs example"
      >
        {_.map(tabs, (tab, index) => (
          <Tab key={index.toString()} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
    </>
  );
}

export default TabBar;
