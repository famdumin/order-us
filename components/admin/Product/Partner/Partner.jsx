import React from 'react';
import {
  Paper,
  Grid,
  Box,
  Typography,
  Input,
  InputAdornment,
  IconButton,
  TextField,
  Divider,
  Button,
} from '@material-ui/core';
import SearchSharpIcon from '@material-ui/icons/SearchSharp';

function Partner(props) {
  return (
    <Box pt={2} mx={-2}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Paper variant="outlined" square style={{ padding: 16 }}>
            <TextField label="Tên đối tác" required={true} margin="normal" fullWidth />
            <TextField
              label="Email đối tác"
              required={true}
              type="email"
              margin="normal"
              fullWidth
            />
            <TextField
              label="Số điện thoại"
              required={true}
              fullWidth
              margin="normal"
              maxLength={12}
            />
            <TextField label="Địa chỉ" required={true} margin="normal" fullWidth />
            <Box pt={3}>
              <Button variant="contained" color="primary" size="large" fullWidth>
                Thêm đối tác
              </Button>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper variant="outlined" square>
            <Box px={2} py={1.5} display="flex" justifyContent="space-between" alignItems="center">
              <Typography color="primary" variant="h6">
                Danh sách đối tác
              </Typography>
              <Input
                placeholder="Tìm theo tên"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton color="primary">
                      <SearchSharpIcon />
                    </IconButton>
                  </InputAdornment>
                }
              />
            </Box>
            <Divider />
            <Box p={2}>No data</Box>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Partner;
