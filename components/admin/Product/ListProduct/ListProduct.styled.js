import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
  card: {
    display: 'flex',
    alignItems: 'center',
    overflow: 'hidden',
  },
}));
