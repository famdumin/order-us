import React from 'react';
import { Grid, Box, Typography, Card, CardMedia, Chip, Fab } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Link from 'next/link';

// import { ProductContext } from 'components/Admin/contexts';
import { useStyles } from './ListProduct.styled.js';

function ListProduct(props) {
  const classes = useStyles();
  // const { state } = React.useContext(ProductContext);
  return (
    <Box my={2}>
      <Grid container spacing={2}>
        {props.products.map((product) => (
          <Grid key={product.id} item xs={4}>
            <Card variant="outlined" className={classes.card}>
              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                width={90}
                height={90}
                p={0.5}
              >
                <CardMedia
                  component="img"
                  image={product.image}
                  title="Live from space album cover"
                />
              </Box>
              <Box
                display="flex"
                flexDirection="column"
                alignItems="flex-start"
                justifyContent="center"
                flex="1"
                p={0.5}
                style={{ width: `calc(100% - 90px)` }}
              >
                <Typography
                  component="h6"
                  variant="subtitle1"
                  style={{ width: '100%' }}
                  noWrap={true}
                >
                  {product.name}
                </Typography>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignSelf="stretch"
                  alignItems="flex-end"
                >
                  <Box display="flex" flexDirection="column">
                    <Chip label="Mỹ phẩm" color="primary" />
                    <Typography variant="subtitle2">{product.price} vnđ</Typography>
                  </Box>
                  <Box m={0.5}>
                    <Link href={`/admin/products/1/update`}>
                      <Fab
                        style={{ marginRight: 8 }}
                        size="small"
                        color="primary"
                        aria-label="edit"
                      >
                        <EditIcon />
                      </Fab>
                    </Link>
                    <Fab size="small" color="secondary" aria-label="delete">
                      <DeleteIcon />
                    </Fab>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}

export default ListProduct;
