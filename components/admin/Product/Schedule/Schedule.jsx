import React from 'react';
import {
  Paper,
  Grid,
  Box,
  Typography,
  FormControlLabel,
  Checkbox,
  Input,
  InputAdornment,
  IconButton,
  Fab,
  TextField,
  Button,
  Divider,
} from '@material-ui/core';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import SearchSharpIcon from '@material-ui/icons/SearchSharp';

function Schedule(props) {
  return (
    <Box pt={2} mx={-2}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Paper variant="outlined" square style={{ padding: 16 }}>
            <TextField label="Tiêu đề lịch biểu" required={true} margin="normal" fullWidth />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Box py={1}>
                <Grid container spacing={1}>
                  <Grid item xs={6}>
                    <KeyboardDatePicker
                      margin="normal"
                      label="Ngày bắt đầu"
                      format="MM/dd/yyyy"
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                      style={{ width: '100%' }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <KeyboardDatePicker
                      margin="normal"
                      label="Ngày kết thúc"
                      format="MM/dd/yyyy"
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                      style={{ width: '100%' }}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box py={1}>
                <Grid container spacing={1}>
                  <Grid item xs={6}>
                    <KeyboardTimePicker
                      margin="normal"
                      label="Giờ bắt đầu"
                      KeyboardButtonProps={{
                        'aria-label': 'change time',
                      }}
                      style={{ width: '100%' }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <KeyboardTimePicker
                      margin="normal"
                      label="Giờ kết thúc"
                      KeyboardButtonProps={{
                        'aria-label': 'change time',
                      }}
                      style={{ width: '100%' }}
                    />
                  </Grid>
                </Grid>
              </Box>
            </MuiPickersUtilsProvider>
            <Typography color="primary" variant="subtitle1">
              Khung giờ Bắt đầu / Kết thúc lịch biểu trong một ngày
            </Typography>
            <Box py={1} direction="row" display="flex" flex="1" alignItems="center">
              <Box color="primary.main" pr={3}>
                <Typography>Loại lịch biểu:</Typography>
              </Box>
              <FormControlLabel
                control={<Checkbox checked={true} name="checked" color="primary" />}
                label="Lịch biểu"
              />
              <FormControlLabel
                control={<Checkbox checked={true} name="checked" color="primary" />}
                label="Giảm giá"
              />
            </Box>
            <Box py={1}>
              <Typography color="primary">Các ngày lặp lại:</Typography>
              <Box
                display="flex"
                flexDirection="row"
                alignItems="center"
                justifyContent="space-between"
              >
                {['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'].map((day, index) => (
                  <Fab key={index.toString()} size="small" variant="extended">
                    {day}
                  </Fab>
                ))}
                <FormControlLabel
                  control={<Checkbox checked={true} name="checked" color="primary" />}
                  label="Chọn tất cả"
                />
              </Box>
            </Box>
            <Box py={2}>
              <Button variant="contained" color="primary" size="large" fullWidth>
                Thêm lịch biểu
              </Button>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper variant="outlined" square>
            <Box
              direction="row"
              display="flex"
              alignItems="center"
              justifyContent="space-between"
              px={2}
              py={1.5}
            >
              <Typography color="primary" variant="h6">
                Danh sách lịch biểu
              </Typography>
              <Input
                placeholder="Tìm kiếm"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton color="primary">
                      <SearchSharpIcon />
                    </IconButton>
                  </InputAdornment>
                }
              />
            </Box>
            <Divider />
            <Box p={2}>
              <Box color="primary.main" direction="row" display="flex" alignItems="center">
                <Box pr={3}>
                  <Typography>Loại lịch biểu: </Typography>
                </Box>
                <FormControlLabel
                  control={<Checkbox checked={true} name="checked" color="primary" />}
                  label="Lịch biểu"
                />
                <FormControlLabel
                  control={<Checkbox checked={true} name="checked" color="primary" />}
                  label="Giảm giá"
                />
              </Box>
            </Box>
          </Paper>
        </Grid>
      </Grid>
      <Box py={2}>
        <Divider />
      </Box>
      <Paper variant="outlined" square style={{ padding: 16 }}>
        <Typography color="primary" variant="h6" gutterBottom>
          Sản phẩm đang được áp dụng lịch biểu
        </Typography>
        <Typography color="primary" variant="subtitle2">
          Chưa chọn lịch biểu/giảm giá
        </Typography>
      </Paper>
    </Box>
  );
}

export default Schedule;
