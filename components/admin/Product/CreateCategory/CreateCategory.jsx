import React from 'react';
import { Box, Paper, Button, TextField } from '@material-ui/core';
import { useForm } from 'react-hook-form';

import { Upload } from 'components/common';
import { messages } from 'validators';

const CreateCategory = React.memo((props) => {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data) => props.onCreateCategory(data);

  return (
    <Paper variant="outlined" square style={{ padding: 16 }}>
      <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
        <TextField
          variant="outlined"
          label="Tên danh mục"
          name="name"
          required={true}
          fullWidth
          maxLength={50}
          inputRef={register({
            required: messages.requiredField,
            maxLength: messages.maxLength(50),
          })}
          helperText={errors?.name?.message || ' '}
        />
        <Box mx={-2} my={2}>
          <Upload
            name="image"
            error={errors?.image?.message}
            register={register({ required: messages.requiredField })}
          />
        </Box>
        <Button type="submit" variant="contained" color="primary" size="large" fullWidth>
          Xác nhận
        </Button>
      </form>
    </Paper>
  );
});

export default CreateCategory;
