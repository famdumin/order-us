import React from 'react';
import Link from 'next/link';
import {
  Box,
  Button,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Pagination from '@material-ui/lab/Pagination';
import Fab from '@material-ui/core/Fab';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { TabPanel, Upload } from 'components/common';
import {
  TabBar,
  FilterBar,
  ListProduct,
  Schedule,
  Partner,
  Categories,
  CreateCategory,
} from '../../Product';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

function ProductManagement(props) {
  const classes = useStyles();
  return (
    <Paper elevation={3}>
      <TabBar tabs={props.tabs} value={props.tabValue} onChangeTab={props.onChangeTab} />
      <TabPanel value={props.tabValue} index={0}>
        <Paper variant="outlined" square>
          <Box px={2} py={1.5} display="flex" alignItems="center" justifyContent="space-between">
            <Typography color="primary" variant="h6">
              Danh sách sản phẩm
            </Typography>
            <Link href="/admin/products/create">
              <Button variant="contained" color="primary" startIcon={<AddCircleIcon />}>
                Tạo sản phẩm
              </Button>
            </Link>
          </Box>
          <Divider />
          <Box p={2}>
            <FilterBar />
            <ListProduct products={props.products} />
            <Box pt={2} display="flex" justifyContent="flex-end">
              <Pagination count={10} color="primary" />
            </Box>
          </Box>
        </Paper>
      </TabPanel>
      <TabPanel value={props.tabValue} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={props.tabValue} index={2}>
        <TabBar
          tabs={props.scheduleTabs}
          value={props.scheduleTabValue}
          onChangeTab={props.onChangeScheduleTab}
        />
        <TabPanel value={props.scheduleTabValue} index={0}>
          <Partner />
        </TabPanel>
        <TabPanel value={props.scheduleTabValue} index={1}>
          <Schedule />
        </TabPanel>
      </TabPanel>
      <TabPanel value={props.tabValue} index={3}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <CreateCategory onCreateCategory={props.onCreateCategory} />
          </Grid>
          <Grid item xs={6}>
            <Categories
              categories={props.categories}
              onChangePageCategory={props.onChangePageCategory}
            />
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={props.tabValue} index={4}>
        aaaaaaa
      </TabPanel>
    </Paper>
  );
}

export default ProductManagement;
