import React from 'react';
import _ from 'lodash';
import { Box, Paper, Divider, Typography, Card, CardMedia, Fab } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import DeleteIcon from '@material-ui/icons/Delete';

import { useStyles } from './Categories.styled';

const Categories = React.memo((props) => {
  const classes = useStyles();
  const { docs: categories, totalPages, page } = props.categories;

  return (
    <Paper variant="outlined" square>
      <Box px={2} py={1.5}>
        <Typography color="primary" variant="h6">
          Danh sách danh mục
        </Typography>
      </Box>
      <Divider />
      <Box p={2}>
        {_.map(categories, (category) => (
          <Card key={category.id} variant="outlined" className={classes.card}>
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              width={100}
              height={100}
              p={0.5}
            >
              <CardMedia
                component="img"
                image={category.image}
                title="Live from space album cover"
              />
            </Box>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              flex="1"
              p={0.5}
              width={`calc(100% - 100px)`}
            >
              <Typography component="h6" variant="h6" noWrap={true} style={{ flex: 1 }}>
                {category.name}
              </Typography>
              <Fab
                style={{ margin: 8 }}
                size="small"
                color="secondary"
                aria-label="delete"
                // onClick={() => deleteCategory(category.id)}
              >
                <DeleteIcon />
              </Fab>
            </Box>
          </Card>
        ))}
      </Box>
      <Box marginLeft="auto" p={2}>
        <Pagination
          count={totalPages}
          page={page}
          color="primary"
          onChange={props.onChangePageCategory}
        />
      </Box>
    </Paper>
  );
});

export default Categories;
