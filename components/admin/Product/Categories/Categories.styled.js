import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  card: {
    display: 'flex',
    alignItems: 'center',
    overflow: 'hidden',
    '&:not(:first-child)': {
      marginTop: theme.spacing(2),
    },
  },
}));
