import React from 'react';
import {
  Box,
  Typography,
  FormControlLabel,
  Checkbox,
  Input,
  InputAdornment,
  IconButton,
  Button,
  Grid,
} from '@material-ui/core';
import SearchSharpIcon from '@material-ui/icons/SearchSharp';

function FilterBar(props) {
  return (
    <>
      <Box color="primary.main" direction="row" display="flex" alignItems="center">
        <Box pr={3}>
          <Typography>Lọc theo loại sản phẩm:</Typography>
        </Box>
        <FormControlLabel
          control={<Checkbox checked={true} name="checked" color="primary" />}
          label="Whole sale"
        />
        <FormControlLabel
          control={<Checkbox checked={true} name="checked" color="primary" />}
          label="Thương mại"
        />
        <FormControlLabel
          control={<Checkbox checked={true} name="checked" color="primary" />}
          label="Quảng cáo"
        />
      </Box>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <Input
            placeholder="Tìm theo tên"
            fullWidth
            endAdornment={
              <InputAdornment position="end">
                <IconButton color="primary">
                  <SearchSharpIcon />
                </IconButton>
              </InputAdornment>
            }
          />
        </Grid>
        <Grid item xs={3}>
          <Input placeholder="Tìm theo đối tác" fullWidth />
        </Grid>
        <Grid item xs={3}>
          <Button fullWidth variant="contained" color="default">
            Giảm giá
          </Button>
        </Grid>
        <Grid item xs={3}>
          <Button fullWidth variant="contained" color="default">
            Lịch biểu
          </Button>
        </Grid>
      </Grid>
    </>
  );
}

export default FilterBar;
