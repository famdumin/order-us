import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  text: {
    color: theme.palette.error.main,
    transform: 'scale(1)',
    minHeight: 20,
  },
  noError: {
    opacity: 0,
    visibility: 'hidden',
  },
}));
