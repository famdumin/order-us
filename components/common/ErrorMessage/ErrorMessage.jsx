import React from 'react';
import clsx from 'clsx';
import FormHelperText from '@material-ui/core/FormHelperText';

import { useStyles } from './ErrorMessage.styled';

const ErrorMessage = ({ helperText }) => {
  const classes = useStyles();
  return (
    <FormHelperText
      className={clsx(classes.text, {
        [classes.noError]: !helperText,
      })}
    >
      {helperText}
    </FormHelperText>
  );
};

export default ErrorMessage;
