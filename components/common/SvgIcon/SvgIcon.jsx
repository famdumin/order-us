import React from 'react';
import { SvgIcon } from '@material-ui/core';

const Icon = ({ fill = '#011C58', children, ...props }) => (
  <SvgIcon {...props} style={{ fill, height: 18, width: 28 }}>
    {children}
  </SvgIcon>
);

export default Icon;
