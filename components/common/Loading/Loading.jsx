import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { useStyles } from './Loading.styled';

export default function Loading(props) {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <CircularProgress {...props} />
    </div>
  );
}
