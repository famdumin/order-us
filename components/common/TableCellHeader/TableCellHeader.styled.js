import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';

export const CellHeader = withStyles((theme) => ({
  root: {
    fontWeight: '700',
    fontSize: 16,
    color: theme.palette.primary.main,
  },
}))(TableCell);
