import React from 'react';

import { CellHeader } from './TableCellHeader.styled';

const TableCellHeader = ({ children, ...props }) => <CellHeader {...props}>{children}</CellHeader>;

export default TableCellHeader;
