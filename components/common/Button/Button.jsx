import React from 'react';
import Button from '@material-ui/core/Button';

import { useStyles } from './Button.styled';

const ButtonCpn = ({ children, ...props }) => {
  const classes = useStyles(props);
  return (
    <Button
      variant="contained"
      color="primary"
      className={classes.button}
      fullWidth={true}
      {...props}
    >
      {children}
    </Button>
  );
};

export default ButtonCpn;
