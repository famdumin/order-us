import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  button: {
    height: ({ genenal }) => (genenal ? 40 : 'auto'),
    marginTop: ({ genenal }) => (genenal ? 25 : 0),
  },
}));
