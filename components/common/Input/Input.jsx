import React from 'react';

import { BootstrapInput } from './Input.styled';

const Input = ({ id, ...props }) => <BootstrapInput id={id} {...props} />;

export default Input;
