import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

export const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: theme.shape.borderRadius,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: `1px solid ${theme.color.borderColor}`,
    fontSize: 16,
    padding: '10px 8px',
    transition: theme.transitions.create(['border-color']),
    fontFamily: ['Nunito Sans', 'sans-serif'].join(','),
    '&:focus': {
      borderColor: theme.palette.primary.main,
    },
  },
}))(InputBase);
