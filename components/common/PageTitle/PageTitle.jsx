import React from 'react';
import { Box, Typography, IconButton } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Router from 'next/router';

function PageTitle({ isBack = false, title, variant = 'h5', component = 'h3' }) {
  return (
    <Box display="flex" alignItems="center" py={2}>
      {isBack && (
        <IconButton color="primary" aria-label="back" component="span" onClick={Router.back}>
          <ArrowBackIcon />
        </IconButton>
      )}
      <Typography variant={variant} component={component} color="primary">
        {title}
      </Typography>
    </Box>
  );
}

export default PageTitle;
