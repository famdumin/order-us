import Box from '@material-ui/core/Box';
import { makeStyles, styled } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  imageWrapper: {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden',
    postion: 'relative',
  },
  image: {
    display: 'block',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  },
  deleteIconStyle: {
    position: 'absolute',
    right: -10,
    top: -10,
    zIndex: 10,
  },
  error: {
    padding: '0 16px',
    color: theme.palette.secondary.main,
    margin: 0,
    fontSize: '0.75rem',
    marginTop: 3,
    textAlign: 'left',
    lineHeight: 1.66,
  },
}));

export const MyBox = styled(Box)(({ draggable }) => ({
  position: 'relative',
  display: 'inline-flex',
  borderRadius: 2,
  borderWidth: 1,
  borderStyle: draggable ? 'dashed' : 'solid',
  borderColor: '#eaeaea',
  margin: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box',
}));
