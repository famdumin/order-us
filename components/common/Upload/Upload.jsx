import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import Box from '@material-ui/core/Box';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';

import { uuidTranslator } from 'utils';
import { useStyles, MyBox } from './Upload.styled';

const Upload = ({ multiple = false, isMulti = false, register, ...props }) => {
  const classes = useStyles();
  const [files, setFiles] = React.useState([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    multiple,
    onDrop: (acceptedFiles) => {
      const newFile = acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
          id: uuidTranslator.uuid(),
        })
      );
      const result = isMulti ? [...files, ...newFile] : newFile;
      setFiles(result);
    },
  });
  const { ref, ...inputProps } = getInputProps();

  const _handleDelete = React.useCallback(
    (fileId) => {
      const nextFiles = _.filter(files, ({ id }) => id !== fileId);
      setFiles(nextFiles);
      if (!nextFiles.length) ref.current.value = '';
    },
    [files]
  );

  React.useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  const thumbs = _.map(files, (file) => (
    <MyBox key={file.id}>
      <div className={classes.imageWrapper}>
        <img src={file.preview} className={classes.image} />
      </div>
      <CancelRoundedIcon
        onClick={() => _handleDelete(file.id)}
        color="secondary"
        className={classes.deleteIconStyle}
      />
    </MyBox>
  ));

  return (
    <div>
      <Box display="flex" flexDirection="row" flexWrap="wrap" p={1}>
        {thumbs}
        <MyBox
          {...getRootProps()}
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          draggable
        >
          <input
            name={props.name}
            ref={(e) => {
              ref.current = e;
              if (register) register(e);
            }}
            {...inputProps}
          />
          <CloudUploadIcon />
          <span>Upload</span>
        </MyBox>
      </Box>
      <span className={classes.error}>{props.error}</span>
    </div>
  );
};

Upload.propTypes = {
  isMulti: PropTypes.bool,
  multiple: PropTypes.bool,
  name: PropTypes.string,
  register: PropTypes.func,
  error: PropTypes.string,
};

export default Upload;
