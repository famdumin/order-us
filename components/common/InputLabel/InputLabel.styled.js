import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  label: {
    color: theme.palette.primary.main,
    fontWeight: '600',
    fontSize: 16,
    transform: 'scale(1)',
  },
}));
