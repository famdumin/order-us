import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';

import { useStyles } from './InputLabel.styled';

const Label = ({ label, id, ...props }) => {
  const classes = useStyles();

  return (
    <InputLabel shrink htmlFor={id} {...props} className={classes.label}>
      {label}
    </InputLabel>
  );
};

export default Label;
