import React from 'react';
import Select from 'react-select';
import { useTheme } from '@material-ui/core/styles';

const ReactSelect = ({ id, ...props }) => {
  const theme = useTheme();

  const customStyles = {
    container: (base) => ({
      ...base,
      marginTop: props.small ? 0 : 25,
      marginLeft: props.small ? theme.spacing(2) : 0,
      width: props.small ? 200 : '100%',
    }),
    control: (base, { isFocused }) => ({
      ...base,
      borderRadius: theme.shape.borderRadius,
      boxShadow: 'none',
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: isFocused ? theme.palette.primary.main : theme.color.borderColor,
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      padding: 0,
      minHeight: props.small ? 36 : 40,
    }),
    input: (base) => ({
      ...base,
      fontSize: props.small ? 14 : 16,
      color: theme.palette.text.primary,
      fontFamily: `'Nunito Sans', sans-serif`,
      fontWeight: '400',
    }),
    placeholder: (base) => ({
      ...base,
      fontSize: props.small ? 14 : 16,
      color: '#d2bbbb',
    }),
    singleValue: (base) => ({
      ...base,
      color: theme.palette.text.primary,
      fontSize: props.small ? 14 : 16,
    }),
    multipleValue: (base) => ({
      ...base,
      color: theme.palette.text.primary,
      fontSize: props.small ? 14 : 16,
    }),
  };
  return <Select instanceId={`react-select-${id}`} styles={customStyles} {...props} />;
};

export default ReactSelect;
