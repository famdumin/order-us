const { localeSubpaths } = require('next/config').default().publicRuntimeConfig;
const NextI18Next = require('next-i18next').default;

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'vi',
  otherLanguages: ['kr'],
  localePath: typeof window === 'undefined' ? 'public/locales' : 'locales',
  localeSubpaths,
});

const { appWithTranslation, withTranslation } = NextI18NextInstance;

const i18n = (module.exports = NextI18NextInstance);
i18n.appWithTranslation = appWithTranslation;
i18n.withTranslation = withTranslation;
