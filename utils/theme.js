import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#01287d',
      main: '#011C58',
      dark: '#011033',
      contrastText: '#fff',
    },
    secondary: {
      main: '#e40606',
      dark: '#da8200',
    },
    text: {
      primary: '#4f4f4f',
    },
  },
  color: {
    blue: '#011c58',
    red: '#c70808',
    gray: '#4f4f4f',
    white: '#fff',
    borderColor: '#BDBDBD',
    mainGradient: 'linear-gradient(136.54deg, #011c58 10.78%, #c70808 93.62%)',
  },
  typography: {
    fontFamily: `'Nunito Sans', sans-serif`,
  },
  overrides: {
    MuiFormHelperText: {
      root: {
        color: '#e40606',
      },
      contained: {
        marginLeft: 0,
        marginRight: 0,
      },
    },
    MuiButton: {
      label: {
        textTransform: 'initial',
      },
    },
    MuiPagination: {
      ul: {
        justifyContent: 'flex-end',
      },
    },
  },
});
