import { toast } from './toast';
import { popup } from './sweetAlert';

export function mutationHelper({ hook, variables, successMessage = 'Thành công!', callback }) {
  hook({
    ...(variables && { variables }),
    update: (_, mutationResult) => {
      const { success, result } = mutationResult.data[Object.keys(mutationResult.data)[0]];
      success && toast.success(successMessage);
      callback && callback(result);
    },
  }).catch((err) => {
    popup.error(err.graphQLErrors.map((error) => error.message).join('/n'));
  });
}
