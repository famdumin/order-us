export const validate = (rules) => {
  let outputRules = {};

  for (let i = 0; i <= rules.length; i++) {
    outputRules = { ...outputRules, ...rules[i] };
  }

  return outputRules;
};
