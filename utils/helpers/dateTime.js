import moment from 'moment-timezone';
import { i18n } from 'utils/i18n';

/**
 *
 * @param {Date} dateData your formated date data or you can use Date object from JavaScript to create one
 * @param {Object} options second param just is a normal object where you can pass your config data
 */
export const dateFormat = (
  dateData,
  { format = 'dddd L HH:mm', locale = i18n.language, timezone = 'Asia/Ho_Chi_Minh' } = {}
) => {
  moment.locale(locale);
  return moment(dateData).tz(timezone).format(format);
};

export const dateToTime = (date) => {
  const hour = ('0' + date.getHours()).slice(-2);
  const minute = ('0' + date.getMinutes()).slice(-2);

  return `${hour}:${minute}`;
};

/**
 *
 * @param {Time} timeData your formated time data
 * @param {Object} options second param just is a normal object where you can pass your config data
 */
export const timeFormat = (timeData, { format = 'hh:mm A', locale = i18n.language } = {}) => {
  moment.locale(locale);
  return moment(timeData, 'HH:mm:ss', true).format(format);
};

export const isValidDate = (date) => date instanceof Date && !isNaN(date);
