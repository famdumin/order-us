import React, { isValidElement } from 'react';
import isString from 'lodash/isString';
import isFunction from 'lodash/isFunction';
import { toast as toastRaw } from 'react-toastify';

import SVGSuccess from 'assets/icons/success.svg';
import SVGError from 'assets/icons/error.svg';

/*
  Calling these toasts most likely happens in the UI 100% of the time.
  So it is safe to render components/elements as toasts.
*/

// Keeping all the toast ids used throughout the app here so we can easily manage/update over time
// This used to show only one toast at a time so the user doesn't get spammed with toast popups
export const toastIds = {
  // APP
  internetOnline: 'internet-online',
  internetOffline: 'internet-offline',
  retryInternet: 'internet-retry',
};

// Note: this toast && is a conditional escape hatch for unit testing to avoid an error.
const getDefaultOptions = (options) => ({
  position: toastRaw && toastRaw.POSITION.BOTTOM_RIGHT,
  ...options,
});

const Toast = ({ children, success, error, info, warning }) => {
  let componentChildren;

  // Sometimes we are having an "object is not valid as a react child" error and children magically becomes an API error response, so we must use this fallback string
  if (!isValidElement(children) && !isString(children)) {
    componentChildren = 'An error occurred';
  } else {
    componentChildren = children;
  }

  let Icon = SVGSuccess;

  if (success) Icon = SVGSuccess;
  if (error) Icon = SVGError;
  if (info) Icon = SVGSuccess;
  if (warning) Icon = SVGError;

  const styles = {
    wrapper: { display: 'flex', alignItems: 'center' },
    icon: {
      width: '20px',
      height: '20px',
      marginRight: '12px',
    },
  };
  return (
    <div style={styles.wrapper}>
      <img src={Icon} style={styles.icon} />
      <div>{componentChildren}</div>
    </div>
  );
};

const toaster = (function () {
  // Attempt to remove a duplicate toast if it is on the screen
  const ensurePreviousToastIsRemoved = (toastId) => {
    if (toastId) {
      if (toastRaw.isActive(toastId)) {
        toastRaw.dismiss(toastId);
      }
    }
  };

  // Try to get the toast id if provided from options
  const attemptGetToastId = (msg, opts) => {
    let toastId;
    if (opts && isString(opts.toastId)) {
      toastId = opts.toastId;
    } else if (isString(msg)) {
      // We'll just make the string the id by default if its a string
      toastId = msg;
    }
    return toastId;
  };

  const handleToast = (type) => (msg, opts) => {
    const toastFn = toastRaw[type];

    if (isFunction(toastFn)) {
      const toastProps = {};
      const progressClassName = 'mytoast-process';
      let className = '';

      const additionalOptions = {};
      const toastId = attemptGetToastId(msg, opts);

      if (toastId) additionalOptions.toastId = toastId;
      // Makes sure that the previous toast is removed by using the id, if its still on the screen
      ensurePreviousToastIsRemoved(toastId);

      // Apply the type of toast and its props
      switch (type) {
        case 'success':
          toastProps.success = true;
          className = 'mytoast mytoast__success';
          break;
        case 'error':
          toastProps.error = true;
          className = 'mytoast mytoast__error';
          break;
        case 'info':
          toastProps.info = true;
          className = 'toast-info';
          break;
        case 'warn':
          toastProps.warning = true;
          className - 'toast-warn';
          break;
        default:
          toastProps.default = true;
          className = 'toast-default';
          break;
      }

      toastFn(<Toast {...toastProps}>{msg}</Toast>, {
        className,
        progressClassName,
        ...getDefaultOptions(),
        ...opts,
        ...additionalOptions,
      });
    }
  };
  return {
    success: handleToast('success'),
    error: handleToast('error'),
    info: handleToast('info'),
    warn: handleToast('warn'),
    default: handleToast('default'),
  };
})();

export const toast = {
  success: toaster.success,
  warn: toaster.warn,
  error: toaster.error,
  info: toaster.info,
  default: toaster.default,
};
