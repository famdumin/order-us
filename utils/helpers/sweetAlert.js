import Swal from 'sweetalert2';

import nyanIcon from 'assets/images/nyan-cat.gif';
import treeIcon from 'assets/images/trees.png';

const popper = (function () {
  const handlePopup = (type) => (msg, opts) => {
    Swal.fire({
      icon: type,
      text: msg,
      customClass: {
        container: 'swal2-1300',
      },
      ...(type === 'error' && {
        background: `#fff url(${treeIcon})`,
        backdrop: `
                        rgba(0,0,123,0.4)
                        url(${nyanIcon})
                        left top
                        no-repeat
                    `,
      }),
      ...opts,
    });
  };

  return {
    success: handlePopup('success'),
    error: handlePopup('error'),
    info: handlePopup('info'),
    warning: handlePopup('warning'),
    question: handlePopup('question'),
  };
})();

export const popup = {
  success: popper.success,
  warn: popper.warning,
  error: popper.error,
  info: popper.info,
  question: popper.question,
};
