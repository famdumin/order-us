import short from 'short-uuid';

import config from '../../config';

export * from './toast';
// export * from './sweetAlert';
// export * from './api';
// export * from './dateTime';
// export * from './urlHelper';
// export * from './moneyFormat';
export * from './localStorage';
// export * from './validate';
// export * from './product/calculatePrice';
// export * from './ConfirmDialog';

export function getImageHost(url) {
  if (!url) {
    return '';
  }
  if (url.match(/^(?:[data]{4}:(text|image|application)\/[a-z]*)/)) {
    return url;
  }
  return url.includes('http') ? encodeURI(url) : `${config.IMAGE_HOST}${encodeURIComponent(url)}`;
}

export function isJSON(item) {
  item = typeof item !== 'string' ? JSON.stringify(item) : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === 'object' && item !== null) {
    return true;
  }

  return false;
}

export const uuidTranslator = short();

export const regexSearchMapper = (str) => {
  str = str.replace(
    /a|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,
    '(a|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)'
  );
  str = str.replace(/e|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, '(e|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)');
  str = str.replace(/i|ì|í|ị|ỉ|ĩ/g, '(i|ì|í|ị|ỉ|ĩ)');
  str = str.replace(
    /o|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,
    '(o|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)'
  );
  str = str.replace(/u|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, '(u|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)');
  str = str.replace(/y|ỳ|ý|ỵ|ỷ|ỹ/g, '(y|ỳ|ý|ỵ|ỷ|ỹ)');
  str = str.replace(/d|đ/g, '(d|đ)');
  str = str.replace(
    /A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,
    '(A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)'
  );
  str = str.replace(/E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, '(E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)');
  str = str.replace(/I|Ì|Í|Ị|Ỉ|Ĩ/g, '(I|Ì|Í|Ị|Ỉ|Ĩ)');
  str = str.replace(
    /O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,
    '(O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)'
  );
  str = str.replace(/U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, '(U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)');
  str = str.replace(/Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, '(Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ)');
  str = str.replace(/D|Đ/g, '(D|Đ)');

  return str;
};

export const truncateWords = (sentence = '', limit = 0, tail = '...') => {
  const words = sentence.split(' ');

  if (limit === 0 || limit >= words.length) {
    return sentence;
  }

  const truncated = words.slice(0, limit);
  return `${truncated.join(' ')}${tail}`;
};

export const isDev = process.env.NODE_ENV === 'development';

export const getLastElementInArrayGroup = (arr, key) => {
  const output = [];

  const splittedObj = arr.reduce((acc, item) => {
    if (!acc[item[key]]) {
      acc[item[key]] = [];
    }

    acc[item[key]].push(item);
    return acc;
  }, {});

  for (const k in splittedObj) {
    const length = splittedObj[k].length;
    output.push(splittedObj[k][length - 1]);
  }

  return output;
};

export const sumArrayNumber = (arr) =>
  arr.reduce(function (acc, val) {
    return acc + val;
  }, 0);
